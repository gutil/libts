/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libts
 * FILE NAME: manage_distrib.c
 *
 * CONTRIBUTORS: Nicolas FLIPO, Lauriane VILMIN, Shuaitao WANG,
 *               Baptiste LABARTHE, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: Management of time series including linear
 * interpolation for holes as well as convertions back and forth between
 * julian days and calendar dates.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libts Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>

/**\fn int find_id(double *rawd,double val,int idmi, int idmax,FILE *fp)
 *\brief return id value of val in raw distrib
 *
 *
 */
int find_id(double *rawd, double val, int idmi, int idmax, FILE *fp) {

  int id = 0;
  double val_inf, val_sup;
  double id_min, id_max, idint, irefmi, irefma;
  int difid = CODE, diftmp;
  int nit = 0;
  int nitmax;

  nitmax = idmax - idmi;
  // NF create a temporary variable to avoid stack overflow;
  irefma = idmax;
  irefmi = idmi;
  // id=idmi;
  difid = idmax - idmi;
  while (difid > 0) {
    nit++;
    if (nit > (nitmax + 1)) {
      LP_printf(fp, "ERROR in libts%f: In find_id() infinite while....Check the algorithm\n", VERSION_TS);
      LP_printf(fp, "libts%f: Pinting the problematic raw_distrib for val = %f within [%f-%f]:\n", VERSION_TS, val, val_inf, val_sup);
      for (id = irefmi; id < irefma; id++)
        LP_printf(fp, "%d %f\n", id, rawd[id]);
      LP_error(fp, "libts%f:....Check the algorithm of find_id() irefmi = %d,irefma  = %d, idmi %d,idmax %d \n", VERSION_TS, irefmi, irefma, idmi, idmax);
    }
    id_min = (double)idmi;
    id_max = (double)idmax;
    val_inf = rawd[idmi];
    val_sup = rawd[idmax];
    diftmp = difid;

    if ((val < val_inf) || (fabs(val - val_inf) < EPS_TS)) {
      id = idmi;
      difid = 0;
    } else {
      if ((val > val_sup) || (fabs(val - val_sup) < EPS_TS)) {
        id = idmax + 1;
        difid = 0;
      } else {
        idint = TS_interpol(id_min, val_inf, id_max, val_sup, val);
        // id=floor(idint+EPS_TS);//NF 22/1/2013 bug majeur qui fait sortir la valeur val de son interval et on a alors des bornes min et max interpolee n'importe comment
        id = floor(idint);
        // LP_printf(fp,"\t Difid %d diftmp %d: id trouve %d (floor(%f) interp %f %f, val %f, mi %f ma %f)\n",difid,diftmp,id,idint,id_min,id_max,val,val_inf,val_sup);
        // useful for debugging
#ifdef TS_DEBUG2
        // LP_printf(fp,"\t id trouve %d pour val %f (floor(%f) interp (%f,%f) (%f,%f))\n",id,val,idint,id_min,val_inf,id_max,val_sup);
#endif

        if ((val > rawd[id]) || (fabs(val - rawd[id]) < EPS_TS)) {
          idmi = id;
        } else {
          idmax = id;
        }

        difid = idmax - idmi;
        /*On est alors bloqué sur un intervalle*/
        if (difid == diftmp) {
          if (difid == 1) {
            // if ((val>rawd[idmi])||(fabs(val-rawd[idmi])<EPS_TS))
            if (fabs(val - rawd[idmi]) < EPS_TS) // NF 22/1/2013 pb algo
              id = idmi;                         // NF 22/1/2013 pb algo id=idmax;
            else
              id = idmax; // NF 22/1/2013 pb algoid=idmi;
            // id=idmax;//NF 22/1/2013 pb algo id=idmax;
            // else id=idmi;//NF 22/1/2013 pb algoid=idmi;
            difid = 0;
          } else {
            if (val < rawd[id + 1])
              idmax = id + 1;
            else
              idmi = id + 1;
            difid = idmax - idmi;
          }
        }
      }
    }
  }

  // NF 10/12/2012 suppression de la recursivite qui genere des stack overflow!!!!!

  /* id_min=(double)idmi;
  id_max=(double)idmax;
  val_inf=rawd[idmi];
  val_sup=rawd[idmax];

  //Useful for debugging
#ifdef TS_DEBUG
  // LP_printf(fp,"Looking %f in [%f-%f] between id [%d-%d] starting %d\n",val,val_inf,val_sup,idmi,idmax,id);
#endif

  if(val<=val_inf)
    id=idmi;
  else {
    if(val>=val_sup)
      id=idmax+1;
    else {
      idint=TS_interpol(id_min,val_inf,id_max,val_sup,val);
      idtmp=floor(idint+EPS_TS);
      //useful for debugging
#ifdef TS_DEBUG
      //  LP_printf(fp,"\t id trouve %d (floor(%f) interp %f %f)\n",idtmp,idint,id_min,id_max);
#endif


      if(idtmp!=id)
        {
          //if(val>rawd[idtmp]) id=find_id(rawd,val,idtmp,idmax,fp);
          //else id=find_id(rawd,val,idmi,idtmp,fp);
          if(val>rawd[idtmp]) id=find_id(rstack,val,idtmp,idmax,fp);
          else id=find_id(rstack,val,idmi,idtmp,fp);
        }
      else
        {
          //En cas d'interpolation dans une gamme tres fine, il faut quand meme verifier que l'id est bon
          if (val>rawd[idtmp]) {
            idtmp++;
            //id=find_id(rawd,val,idtmp,idmax,fp);
            id=find_id(rstack,val,idtmp,idmax,fp);
          }

        }
    }
    }*/

  return id;
}

/**\fn void reorder_distrib(double *rawd,int id,int it,double val)
 *\brief reorder value in a raw distrib and positions val at id, while other values move forward
 *
 *
 */
void reorder_distrib(double *rawd, int id, int it, double val) {
  int i;

  for (i = it; i > id; i--)
    rawd[i] = rawd[i - 1];
  rawd[id] = val;
}
/**\fn void position_val(double *rawd,double val,int it,FILE *fp)
 *\brief Function to place value val at it position and reorder rawd if necessary
 */
int position_val(double *rawd, double val, int it, FILE *fp) {
  int id;
  double mini, maxi;

  rawd[it] = val;
  id = it - 1;
  if (id > 0) {
    id = find_id(rawd, val, 0, id, fp);
    // Useful for debugging
#ifdef TS_DEBUG
    // LP_printf(fp,"\t***It %d : Id found %d\n",it,id);
#endif
    if (id != it) {
      reorder_distrib(rawd, id, it, val);
      // Useful for debugging
#ifdef TS_DEBUG
      // LP_printf(fp,"\t***Reordering!!\n");
#endif
    }
  } else {
    if (id == 0) {
      mini = TS_min(rawd[0], val);
      maxi = TS_max(rawd[0], val);
      rawd[0] = mini;
      rawd[it] = maxi;
    }
  }
  return id;
}
/**\fn s_distrib_ts *create_distrib(s_ft *pftmp,int nclass,FILE *fp)
 *\brief Creation d'un objet distrib a partir d'un pointeur vers ft
 *
 *This functions is thus defining min,max,average,std,class lag,and finally the statistical distribution of pft.
 */
s_distrib_ts *create_distrib(s_ft *pftmp, int nclass, FILE *fp) {
  s_distrib_ts *pdistrib;
  double maxi = SMALL_TS;
  double mini = BIG_TS;
  int nval = 0;
  int ni;
  int i, j;
  int it = 0;
  double val;
  double borne;
  s_ft *pft;
  int id;
  int nval_check = 0;

  if (nclass == CODE_TS)
    nclass = NCLASS_TS;

  pft = TS_browse_ft(pftmp, BEGINNING_TS);

  pdistrib = new_distrib_ts();
  bzero((char *)pdistrib, sizeof(s_distrib_ts));
  // pft=TS_browse_ft(pft,BEGINNING_TS);
  pdistrib->pft = pft;
  pdistrib->average = TS_average(pft);
  pdistrib->std = TS_stdev(pft, pdistrib->average);

  /*Defining min and max*/

  while (pft != NULL) {
    nval++;
    mini = TS_min(mini, pft->ft);
    maxi = TS_max(maxi, pft->ft);
    pft = pft->next;
  }
  pdistrib->min = mini;
  pdistrib->max = maxi;
  pdistrib->nval = nval;

  if (nclass > nval) {
    LP_warning(fp, "In libts%4.2f %s line %d: Not enough values to create %d classes --> nclass changed to %d\n", VERSION_TS, __FILE__, __LINE__, nclass, nval);
    nclass = nval; // NF 27/10/2014 To avoid a bug in this case
  }

  pdistrib->nclass = nclass;

  /*defining the lag*/
  pdistrib->class_lag = pdistrib->max;
  pdistrib->class_lag -= pdistrib->min;
  pdistrib->class_lag /= nclass;

#ifdef TS_DEBUG
  LP_printf(fp, "libts%3.1f: %d values, with min = %f, max = %f => class_lag = %f\n", VERSION_TS, pdistrib->nval, pdistrib->min, pdistrib->max, pdistrib->class_lag); // NF useful for debugging
#endif
  /*allocating the memory*/

  /*pdistrib->raw_distrib=((double*)calloc(pdistrib->nval,sizeof(double)));
  pdistrib->class_distrib=((double **)calloc(NCOL_CLASS,sizeof (double *)));
  for (i=0;i<NCOL_CLASS;i++)
  pdistrib->class_distrib[i]=((double*)calloc(pdistrib->nclass,sizeof(double)));*/

  pdistrib->raw_distrib = ((double *)malloc(pdistrib->nval * sizeof(double)));
  // pdistrib->class_distrib=((double **)malloc(NCOL_CLASS*sizeof (double *)));
  for (i = 0; i < NCOL_CLASS; i++)
    pdistrib->class_distrib[i] = ((double *)malloc(pdistrib->nclass * sizeof(double)));
  /*ordering the raw distribution*/
  pft = pdistrib->pft;
  while (pft != NULL) {
    id = position_val(pdistrib->raw_distrib, pft->ft, it, fp); // NF I do not need id here, but I will in verify_ts
    it++;
    pft = pft->next;
  }
  /*Creating the statistical distrib itself*/
  if (fabs(mini - maxi) > EPS_TS) {
    it = 0;
    for (i = 0; i < nclass; i++) {
      j = 0;
      borne = pdistrib->min;
      borne += (i + 1) * pdistrib->class_lag;
      val = pdistrib->raw_distrib[it + j];
      while ((val < borne) && ((it + j + 1) < pdistrib->nval)) {
        j++;
        val = pdistrib->raw_distrib[it + j];
      }
      it += j;
#ifdef TS_DEBUG
      //	LP_printf(fp,"lts%3.1f: Number checked = %d / %d\n",VERSION_TS,it,nval);
#endif
      pdistrib->class_distrib[NVAL_CLASS][i] = (double)j;
    } // end for i

    // Il faut faire un check sur le nb val max. Cas où toutes les valeurs valent la borne max

    for (i = 0; i < nclass; i++)
      nval_check += pdistrib->class_distrib[NVAL_CLASS][i];
    nval_check = nval - nval_check;
    pdistrib->class_distrib[NVAL_CLASS][nclass - 1] += nval_check; //+1 pour la derniere classe car max n'y est pas compte
  } else {                                                         // Cas d'une distribution a valeur constante
#ifdef DEBUG
    LP_warning(fp, "#lts%3.1f : Constant statistical distribution %f\n", VERSION_TS, mini);
#endif
    ni = floor(nval / nclass);
    for (i = 0; i < (nclass - 1); i++)
      pdistrib->class_distrib[NVAL_CLASS][i] = ni;
    pdistrib->class_distrib[NVAL_CLASS][nclass - 1] = nval - (nclass - 1) * ni;
  }

  /*class in frequency */
  j = 0;
  for (i = 0; i < nclass; i++)
    j += pdistrib->class_distrib[NVAL_CLASS][i];
  for (i = 0; i < nclass; i++) {
    if (j == 0)
      LP_error(fp, "In libts1 manage_distrib.c create_distrib(): Impossible to divide by zero\n");
    pdistrib->class_distrib[FREQ_CLASS][i] = pdistrib->class_distrib[NVAL_CLASS][i] / j;
  }
  it = 0;
  for (i = 0; i < nclass; i++) {
    it += pdistrib->class_distrib[NVAL_CLASS][i];
    pdistrib->class_distrib[CUMUL_CLASS][i] = (double)it;
    pdistrib->class_distrib[FREQ_CUMUL][i] = pdistrib->class_distrib[CUMUL_CLASS][i] / nval;
  }
  return pdistrib;
}
/**\fn int check_quanti(int quant,int nval)
 *\brief check the quantile consistency
 *
 */
int check_quanti(int quant, int nval) {
  int error = NO_TS;
  float check;

  if (quant > 50)
    quant = 100 - quant;
  check = (float)nval;
  check /= quant;
  if (check < 1)
    error = YES_TS;

  return error;
}
/**\fn double carac_distrib(s_distrib_ts *pdistrib,double quant,FILE *fp)
 * \brief calculates quantile if possible
 *\return quaitle value or error code (-9999)
 */
double carac_distrib(s_distrib_ts *pdistrib, double quant, FILE *fp) {
  int error;
  double valq;
  double mini, maxi;
  valq = (double)CODE_TS;

  mini = pdistrib->min;
  maxi = pdistrib->max;

  error = check_quanti(quant, pdistrib->nval);
  if (error == YES_TS) { // NF 7/9/2014 Corrected because does not work for short time series
    LP_warning(fp, "In lts%4.2f: Few values to calculate quantile %d: ignored but might cause further problems in outputs of the calling code\n", VERSION_TS, quant);
  }
  if (fabs(mini - maxi) > EPS_TS) {
    valq = find_quantile(pdistrib, quant);
  } else
    valq = CODE_TS;
  return valq;
}
/**\fn void TS_free_distrib(s_distrib_ts *pdistrib,FILE *fp)
 * \brief free distrib pointeur
 */
void TS_free_distrib(s_distrib_ts *pdistrib, FILE *fp) {
  int i;

  free(pdistrib->raw_distrib);
  free(pdistrib->quant);
  for (i = 0; i < NCOL_CLASS; i++)
    free(pdistrib->class_distrib[i]);

#ifdef TS_DEBUG
  LP_printf(fp, "Distribution de-allocated\n");
#endif
  free(pdistrib);
}

/**\fn void double *TS_quant_defaut_ini(int *nq,double *quant,FILE *fp)
 * \brief define default quantile values
 */
double *TS_quant_defaut_ini(int *nq, double *quant, FILE *fp) {
  if (*nq == CODE_TS) {
    *nq = NQUANT_TS;
    quant = (double *)malloc((*nq) * sizeof(double));
    // quant=(int*) calloc(*nq,sizeof(int));
    quant[Q2] = 2;
    quant[Q5] = 5;
    quant[Q10] = 10;
    quant[Q25] = 25;
    quant[Q50] = 50;
    quant[Q75] = 75;
    quant[Q90] = 90;
    quant[Q95] = 95;
    quant[Q98] = 98;
    // valq=(double*) calloc(*nq,sizeof(double));
    // valq=(double*) malloc((*nq)*sizeof(double));
  } else
    LP_error(fp, "In libts%4.2f: call to TS_quant_defaut_ini() while nq is not equal to NQUANT_TS\n", VERSION_TS);
  return quant;
}

/**\fn double *TS_carac_raw_ts(s_ft *pft,int *nclass,int *nq,double *quant,double *valq,FILE *fp,int coupled)
 *\brief define quantile and statistical distribution on unclassed pft
 *\return quantile tabular
 */

double *TS_carac_raw_ts(s_ft *pft, int *nclass, int *nq, double *quant, double *valq, FILE *fp, int coupled) {
  int i;
  s_distrib_ts *pdistrib;
  // int nclass;

  valq = (double *)malloc((*nq) * sizeof(double));

  if (*nclass == CODE_TS)
    *nclass = NCLASS_TS;
  pdistrib = create_distrib(pft, *nclass, fp);

#ifdef TS_DEBUG
  TS_print_raw_distrib(pdistrib, fp);
  TS_print_carac_distrib(pdistrib, fp);
#endif

  for (i = 0; i < *nq; i++) {
    // LP_printf(fp,"%d %f ",i,valq[i]);
    valq[i] = carac_distrib(pdistrib, quant[i], fp);
    //  LP_printf(fp,"%f ",valq[i]);
  }
  pdistrib->quant = valq;

#ifdef TS_DEBUG
  TS_print_header_quantiles(nq, fp, quant); // NF 7/9/2014 There was a small bugg. In case the default quantile are calculated, we must print quant and not valq!
  TS_print_quantiles(nq, valq, fp);
#endif
  if (coupled == YES_TS)
    TS_print_all_stats_ts(pdistrib, nq, fp);

  TS_free_distrib(pdistrib, fp);

  return valq;
}
