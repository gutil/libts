/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libts
 * FILE NAME: struct_ts.h
 *
 * CONTRIBUTORS: Nicolas FLIPO, Lauriane VILMIN, Shuaitao WANG,
 *               Baptiste LABARTHE, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: Management of time series including linear
 * interpolation for holes as well as convertions back and forth between
 * julian days and calendar dates.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libts Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.htmlml
 *
 *------------------------------------------------------------------------------*/

typedef struct function_t s_ft;
typedef struct distrib_ts s_distrib_ts;
typedef struct date_ts s_date_ts;
typedef struct carac_ts s_carac_ts;
// typedef struct settings_ts s_settings_ts;

/**
 *\struct function_t
 *\brief Description of the evolution of a variable versus another, exeample (t,f(t))
 */
struct function_t {
  double t;   /*!< Independant variable */
  double ft;  /*!< Function's value for the variable t */
  s_ft *next; /*!< Pointer towards the next time step */
  s_ft *prev; /*!< Pointer towards the prev time step */
};
/**
 *\struct distrib_t
 *\brief Description of the statistics of a variable array
 */
struct distrib_ts {
  s_ft *pft;                         /*!<Pointeur to the first element of the ts, ie navigation with next */
  int nval;                          /*!<number of values*/
  double *raw_distrib;               /*!<pointer to the array containing the distribution*/
  double class_lag;                  /*!<value of the lag*/
  int nclass;                        /*!<number of classes*/
  double *class_distrib[NCOL_CLASS]; /*!<pointer to the array containing the number of values by lags*/
  double min;                        /*!<min of the serie*/
  double max;                        /*!<max of the serie*/
  double average;                    /*!<average of the serie*/
  double std;                        /*!<std of the serie*/
  int nquant;                        /*!<number of quantiles*/
  double *quant;                     /*!< quantiles values*/
};

/*struct settings_ts {
  char ts_sep[STRING_LENGTH_TS];
  }*/

/**
 *\struct date_ts
 *\brief structur containing a date
 */
struct date_ts {
  int dd;   /*!<day*/
  int mm;   /*!<month*/
  int yyyy; /*!<year*/
  int hh;   /*!<hour*/
  int min;  /*!<minute*/
  int ss;   /*!<seconde*/
};

/**
 *\struct carac_ts
 *\brief structur containing the temporal caracteristics of a ts
 */
struct carac_ts {
  s_date_ts *pd[NEXTREMA_TS];     /*!<first and last date*/
  double julian_day[NEXTREMA_TS]; /*!<first and last date in julian days*/
  int nyr;                        /*<number of years*/
};
#define new_function() ((s_ft *)malloc(sizeof(s_ft)))
#define new_distrib_ts() ((s_distrib_ts *)malloc(sizeof(s_distrib_ts)))
#define new_date_ts() ((s_date_ts *)malloc(sizeof(s_date_ts)))
#define new_carac_ts() ((s_carac_ts *)malloc(sizeof(s_carac_ts)))
