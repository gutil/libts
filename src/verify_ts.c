/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libts
 * FILE NAME: verify_ts.c
 *
 * CONTRIBUTORS: Nicolas FLIPO, Lauriane VILMIN, Shuaitao WANG,
 *               Baptiste LABARTHE, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: Management of time series including linear
 * interpolation for holes as well as convertions back and forth between
 * julian days and calendar dates.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libts Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>

/**\fn int TS_length_ts(s_ft *ptmp)
 *\brief calculate the length of a pft
 *
 */
int TS_length_ts(s_ft *ptmp) {

  s_ft *pft = NULL;
  pft = ptmp;
  int count = 0;

  pft = TS_browse_ft(pft, BEGINNING_TS);

  while (pft != NULL) {
    count++;
    pft = pft->next;
  }

  return count;
}

/**\fn s_ft *TS_reorder_t(s_ft *pft,FILE *flog)
 *\brief reorder pft following a growing t from tmin to tmax
 *
 */

s_ft *TS_reorder_t(s_ft *pft, FILE *flog) {
  s_distrib_ts *pdis;
  double maxi = SMALL_TS;
  double mini = BIG_TS;
  int nval, it = 0, id = 0;
  double *tmp_t, *tmp_ft;
  s_ft *ptmp;

#ifdef TS_DEBUG
  char name[STRING_LENGTH_LP];
  FILE *fp;

  sprintf(name, "cmp_kro_t.txt");
  fp = fopen(name, "w");
#endif

  ptmp = TS_browse_ft(pft, BEGINNING_TS);

  nval = TS_length_ts(ptmp);
  tmp_t = ((double *)malloc(nval * sizeof(double)));
  tmp_ft = ((double *)malloc(nval * sizeof(double)));
  ptmp = TS_browse_ft(pft, BEGINNING_TS);
  pft = TS_browse_ft(pft, BEGINNING_TS);
  while (ptmp != NULL) {
    id = position_val(tmp_t, ptmp->t, it, flog);
    // if(id>0){
    if ((id != it) && (it > 1)) {
      reorder_distrib(tmp_ft, id, it, ptmp->ft);
    } else {
      if ((it > 1) || (it < 1))
        tmp_ft[it] = ptmp->ft;
      else {
        if (fabs(tmp_t[0] - ptmp->t) > EPS_TS)
          tmp_ft[it] = ptmp->ft;
        else {
          tmp_ft[it] = tmp_ft[0];
          tmp_ft[0] = ptmp->ft;
        }
      }
    }
    it++;
    ptmp = ptmp->next;
  }

#ifdef TS_DEBUG
  LP_printf(fp, "Before Reordering :\n");
  TS_print_ts_with_date(pft, CODE_TS, FR_TS, fp, flog);
#endif

  ptmp = TS_browse_ft(pft, BEGINNING_TS);
  /*Reordering itself*/
  it = 0;
  while (ptmp != NULL) {
    pft->t = tmp_t[it];
    pft->ft = tmp_ft[it];
    it++;
    ptmp = ptmp->next;
    if (ptmp != NULL)
      pft = pft->next;
  }
  pft = TS_browse_ft(pft, BEGINNING_TS);

#ifdef TS_DEBUG
  LP_printf(fp, "After Reordering :\n");
  TS_print_ts_with_date(pft, CODE_TS, FR_TS, fp, flog);
  fclose(fp);
#endif
  // free(tmp_t);
  // free(tmp_ft);
  return pft;
}
