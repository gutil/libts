/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libts
 * FILE NAME: functions_ts.h
 *
 * CONTRIBUTORS: Nicolas FLIPO, Lauriane VILMIN, Shuaitao WANG,
 *               Baptiste LABARTHE, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: Management of time series including linear
 * interpolation for holes as well as convertions back and forth between
 * julian days and calendar dates.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libts Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/*Manage_time_series.c*/
s_ft *TS_init_ft();
s_ft *TS_chain_bck_ts(s_ft *, s_ft *);
s_ft *TS_create_function(double, double);
s_ft *TS_secured_chain_fwd_ts(s_ft *, s_ft *);
s_ft *TS_secured_chain_bck_ts(s_ft *, s_ft *);
s_ft *TS_create_ts_from_file(char *, FILE *);
s_ft *TS_add_pointers(s_ft *, s_ft *, s_ft *);
s_ft *TS_add_functions(s_ft *, s_ft *);
s_ft *TS_secure_add_functions(s_ft *, s_ft *);
s_ft *TS_browse_ft(s_ft *, int);
s_ft *TS_free_ft(s_ft *);
s_ft *TS_free_ts(s_ft *, FILE *);
s_ft *TS_browse_tabulations(double, s_ft *, int, FILE *);
double TS_value_tabulations(double, s_ft *, int, FILE *);
s_ft *TS_multiply_ft_double(s_ft *, double);
s_ft *TS_divide_fts(s_ft *, s_ft *);
s_ft *TS_multiply_fts(s_ft *, s_ft *);
s_ft *TS_mean_fts(s_ft *, s_ft *);
s_ft *TS_substract_fts(s_ft *, s_ft *);
s_ft *TS_division_ft_double(s_ft *, double, FILE *);
s_ft *TS_abs_ft(s_ft *);
s_ft *TS_abs_substract_fts(s_ft *, s_ft *);
void TS_translate_t(s_ft *, double, FILE *);
void TS_auto_translate_t(s_ft *, FILE *);
void TS_translate_ft(s_ft *, double, FILE *);
void TS_mult_t(s_ft *, double, FILE *);
double TS_function_value_t(double, s_ft *, FILE *);
s_ft *TS_function_t_pointed(double, s_ft *, FILE *);
s_ft *TS_compare_time(s_ft *, double);
int TS_is_bissex(int);
s_ft *TS_copy_ft(s_ft *);
s_ft *TS_copy_ts(s_ft *);
double TS_affect_t(double, s_ft *, int);
s_ft *TS_extract_chro(s_ft *, int *, double, double, FILE *);
s_ft *TS_extract_chro_interpolate(s_ft *, double, double, FILE *);

s_ft *ins_pointer_t(s_ft *, s_ft *, int);
s_ft *chain_fwd_ts(s_ft *, s_ft *);
double value_tabulations(double, s_ft *, int);
s_ft *TS_differentiate_ts(s_ft *, double, FILE *);
s_ft *TS_complete_time_series(s_ft *, s_ft *);
s_date_ts **TS_get_ts_border(s_ft *, int, FILE *);

/*Calculs stat de base stat_ts.c*/
double TS_weighted_average(s_ft *);
double TS_average(s_ft *);
double TS_stdev(s_ft *, double);
double find_quantile(s_distrib_ts *, double);
double correlation(s_ft *, s_ft *, double, double);
void TS_linear_regression(s_ft *, s_ft *, double *, double *, FILE *);
double TS_average_product(s_ft *, s_ft *);
double quadratic_mean(s_ft *);
s_ft *TS_moving_average(s_ft *, double, double, double, double, int, FILE *);
s_ft *TS_create_average_step(s_ft *, double, int, double, double, FILE *);
s_ft *TS_create_averaged_ts(s_ft *, double, int, FILE *);
s_ft **TS_create_stat_step(s_ft *, double, int, double, double, FILE *);
s_ft **TS_create_stat_ts(s_ft *, double, int, FILE *);
s_ft *TS_VCN(s_ft *, double, double, double, double, int, FILE *);
s_ft *TS_min_ft(s_ft *); /*Here TS_min_ft sends a pointer back, because we want to have axess also to the date of the minimum value. */
s_ft *TS_max_ft(s_ft *);
double TS_qmna(s_ft *, double, double, int, double, int, int, FILE *);
s_ft *TS_get_max_annual(s_ft *, int, FILE *);
double TS_sum_ts(s_ft *, FILE *);

double TS_integrate_ts(s_ft *, double, double, FILE *);
double TS_integral_average_ts(s_ft *, double, double, FILE *);

/*Gestion d'une distribution: manage_distrib.c*/
s_distrib_ts *create_distrib(s_ft *, int, FILE *);
double *TS_carac_raw_ts(s_ft *, int *, int *, double *, double *, FILE *, int);
double *TS_quant_defaut_ini(int *, double *, FILE *);
int position_val(double *, double, int, FILE *);
void reorder_distrib(double *, int, int, double);
void TS_free_distrib(s_distrib_ts *pdistrib, FILE *fp);

/*Comparison of TS in cmp_kro.c*/
void TS_cmp_kro(s_ft *, s_ft *, FILE *);
double TS_interpolate_ts(double, s_ft *);
s_ft *TS_create_simultaneous_ts(s_ft *, s_ft *);

/*Basic printing functions in print_ts.c*/
void TS_print_ts(s_ft *, FILE *);
void TS_printf_ts(s_ft *, FILE *);
void TS_print_ts_wsep(s_ft *, char, FILE *);
void TS_print_raw_distrib(s_distrib_ts *, FILE *);
void TS_print_carac_distrib(s_distrib_ts *, FILE *);
void TS_print_header_quantiles(int *, FILE *, double *);
void TS_print_quantiles(int *, double *, FILE *);
void TS_print_all_stats_ts(s_distrib_ts *, int *, FILE *);
void TS_print_in_line_ft_ts(s_ft *, FILE *);
void TS_print_in_column_ft_ts(s_ft *, FILE *);
void TS_print_ts_with_date(s_ft *, int, int, FILE *, FILE *);
void TS_print_ts_with_date_only(s_ft *, int, int, FILE *, FILE *);
void TS_print_ts_with_date_wsep(s_ft *, int, int, char, FILE *, FILE *);
void TS_print_ts_with_date_wsep_hh_mm(s_ft *, int, int, char, FILE *, FILE *);

/*Conversion of integer to names in itos_ts.c*/
char *TS_name_quantile(int, FILE *);
char *TS_name_month(int, FILE *);
char *TS_name_bissex(int);
char *TS_name_order_distrib(int, FILE *);
char *TS_name_time_scale(int, FILE *);
char *TS_name_date_encoding(int, FILE *);
char *TS_name_stat(int, FILE *);

/*Calculates the time in julian days in manage_time.c*/
float TS_def_calc_bissex(int);
int TS_year_type(int, int);
int TS_nb_jour_an(int);
int TS_nday_month(int);
int TS_calculate_nd_month(int, int, int);
int TS_nday_cum_month(int);
int TS_calculate_jour_julien(int, int, int);
int TS_calculate_jour_julien_j(int, int, int, int, FILE *);
double TS_calc_julian_decimal(int, int, int, FILE *);
int TS_process_time(int, int *, int, int, int, FILE *);
int TS_modify_hr(int, int, int, int, FILE *);
int TS_switch_time(int);
int TS_calculate_jour_julien_j_nbday(int, int, int); // NF 13/2/2020 TV=bug for many other library
// double TS_calculate_jour_julien_j_nbday(int,int,int,double); //TVdebug
s_date_ts *TS_convert_julian2date(int, int, FILE *);
s_date_ts *TS_convert_julian2date_decimal(double, int, FILE *);
int TS_date2julian_dd(s_date_ts *, int, FILE *);
double TS_date2julian_dd_hm(s_date_ts *, int, FILE *);
double TS_convert_seconds2days(double tseconds, FILE *flog); // NF 3/12/2020

s_ft *TS_create_from_file_date(char *, int, int, FILE *);
s_ft *TS_create_from_file_date_hh(char *, int, int, int, FILE *);
s_ft *TS_create_from_file_date_hh_mm(char *, int, int, int, FILE *);

void TS_create_monthly_ts(s_ft *, s_ft **, FILE *, int, int, int, int, int);
void TS_check_nb_day(int, int, int, FILE *);
void TS_print_date(s_date_ts *, int, FILE *, FILE *);
void TS_print_date_sep(s_date_ts *, int, char *, FILE *, FILE *);
void TS_print_time(s_date_ts *, FILE *, FILE *);
void TS_print_time_hh_mm(s_date_ts *, FILE *, FILE *);

s_carac_ts *TS_carac_ts(s_ft *, int, FILE *);
void TS_destroy_carac_ts(s_carac_ts *);
void TS_create_annual_ts(s_ft *, s_ft **, FILE *, int, int, int, int, int);
s_ft **TS_cut_ts_annually(s_ft *, int, FILE *); // Here the detection of the length of the time series is automatic and one time series per year is generated
double TS_unit2sec(int, FILE *);

void TS_print_date_from_julian(double t, int yr0, int icode, FILE *fp, FILE *flog);

/*math_function.c*/
double TS_min(double, double);
double TS_max(double, double);
double TS_interpol(double, double, double, double, double);
double TS_avoid_inf(double, double);
double TS_integrate(double, double, double, double);
int TS_is_value_closed_interval(double, double, double); // NG : 01/10/2020

/*In verify_ts.c*/
int TS_length_ts(s_ft *);
s_ft *TS_reorder_t(s_ft *, FILE *);

/*In wodic_cpl.c*/
void TS_print_ts_for_hydsep(s_ft *, FILE *, FILE *);
