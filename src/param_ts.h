/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libts
 * FILE NAME: param_ts.h
 *
 * CONTRIBUTORS: Nicolas FLIPO, Lauriane VILMIN, Shuaitao WANG,
 *               Baptiste LABARTHE, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: Management of time series including linear
 * interpolation for holes as well as convertions back and forth between
 * julian days and calendar dates.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libts Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/* Nb used for tests */
#define CODE_TS -9999
#define CODE CODE_TS
#define EPS_TS 0.000000000001 // modif EPS_TS passe de 10-32 à 10-12
#define EPS2_TS 0.000000001   // NF 4/10/2016 larger EPS needed for monthly calculation
#define INFINITE_DIV_TS 0

enum beginning_end { BEGINNING_TS, END_TS, NEXTREMA_TS };

#define MIDDLE_TS NEXTREMA_TS
#define VERSION_TS 1.74

#define VERSION_TS 1.74

enum class_values { NVAL_CLASS, FREQ_CLASS, CUMUL_CLASS, FREQ_CUMUL, NCOL_CLASS };

#define YES_TS YES_LP
#define NO_TS NO_LP
#define NANSWER_TS NANSWER_LP

#define BIG_TS 999999999
#define SMALL_TS -999999999

enum simul_sim_obs_ts { SIM_TS, OBS_TS, NKRO_TS };

enum quant_list_ts { Q2, Q5, Q10, Q25, Q50, Q75, Q90, Q95, Q98, NQ_TS };

enum months { JANUARY, FEBRUARY, MARCH, APRIL, MAY, JUNE, JULY, AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER, NMONTHS_TS };
enum calc_bissex { NORMAL_TS, CENTURY_TS, FOURCENTURY_TS, NBISSEX_TS };

#define BISSEXTILE_TS 366
#define NDAY_YEAR_TS 365.25

#define STRING_LENGTH_TS STRING_LENGTH_LP
#if defined GCC472 || defined GCC471 || defined GCC445
#define TS_INFINITE FP_INFINITE
#else
#define TS_INFINITE -66666666666666
#endif // end definition TS_INFINITE

enum list_time_scale { DAILY_TS, ANNUAL_TS, MONTHLY_TS, VCN_TS, QMNA_TS, NTIME_SCALE_TS };

#define NPERIOD_MIN_TS 1 // init 3 Minimum number of period necessary to calculate a statistics. To calculate QMNA 5 we need a time series of at least 5*NPERIOD_MIN_TS years

enum date_type { FR_TS, US_TS, MIX_TS, NDATETYPE_TS };
enum day_time { AM_TS, PM_TS, NDAYTIME_TS }; /* defines if it's midnight to noon AM_TS, or noon to midnight PM_TS*/
enum sensors_treatment { HOBBO_TS = 333, NSENSORS_TS };

// Values by default
#define INITIAL_YEAR_JULIAN_DAY_TS 1850 /* Year of beginning for the julian days*/
#define NQUANT_TS 9                     // By default 9 quantiles 2, 5, 10, 25, 50, 75, 90, 95, 98
#define NCLASS_TS 10                    // Number of classes in a distribution by default
#define NVAL_MIN_TS 10                  // Minimal numbers of values to calculate the moving average
#define NVAL_DT_MIN_TS 1                // Minimal numbers of values to calculate an averaged time series with a given time step. ie at least one value is required
#define THETA_TS 0.5                    // By default the moving average is centered
#define LNEIGH_TS 30                    // By default the neighbourhood for the moving average is set up to 30 units of time (this is done for the VCN 30)
#define QUANTILE_TS 20                  // The quantile looked up by default is 20 percent corresponding to a 5 yr return period of time

#define HOUR_SCALE_TS 24
#define HOUR_START_TS 0
#define DAY_TIME_TS AM_TS
#define NSEC_DAY_TS 86400.
#define CONV_SEC_DDEC_TS 1 / NSEC_DAY_TS
#define NMIN_HOUR_TS 60.
#define NSEC_MIN_TS 60.
#define CONV_SEC_MIN_TS 1 / NSEC_MIN_TS
#define NSEC_HOUR_TS (NMIN_HOUR_TS * NSEC_MIN_TS)
#define CONV_SEC_HOUR_TS 1 / NSEC_HOUR_TS

#define HOBBO_SCALE_TS 12
#define HOBBO_OFFSET_TS 0
#define HOBBO_DAY_TIME_TS CODE_TS
enum unit_t { SEC_TS, MIN_TS, HOUR_TS, DAY_TS, MONTH_TS, YEAR_TS, NUNIT_TS };

enum simple_statistics_ts { AVERAGE_TS, STD_TS, NSTAT_TS };

#define TSTEP_STAT_TS 1.
