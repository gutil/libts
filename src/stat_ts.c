/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libts
 * FILE NAME: stat_ts.c
 *
 * CONTRIBUTORS: Nicolas FLIPO, Lauriane VILMIN, Shuaitao WANG,
 *               Baptiste LABARTHE, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: Management of time series including linear
 * interpolation for holes as well as convertions back and forth between
 * julian days and calendar dates.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libts Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"

/**\fn s_ft *TS_moving_average(s_ft *pft,double lneigh, double theta,double t0,double tf,int nval_min,FILE *fp)
 *\brief calculate the  moving average of the time serie pft between t0 and tf.\\
For each  point t, the average value affected at t is calculated for a neighborhood of lneigh, centered around t by the parameter theta (0->downstream, 1 -> upstream, 0.5->fully centered). nval_min is the minimum number of values that have to be found in lneigh to calculate the average
 */
s_ft *TS_moving_average(s_ft *pft, double lneigh, double theta, double t0, double tf, int nval_min, FILE *fp) {
  s_ft *ptmp;
  s_ft *pft_mv1, *pft_mv2;
  double delta;
  double time_up, t, t_min;
  int nval = 0;

  if (fabs(lneigh - CODE_TS) < EPS_TS)
    lneigh = LNEIGH_TS;
  if (fabs(theta - CODE_TS) < EPS_TS)
    theta = THETA_TS;
  if (fabs(nval_min - CODE_TS) < EPS_TS)
    nval_min = NVAL_MIN_TS;

  // printf("lneigh=%f\n theta=%f\n t0=%f\n tf=%f\n nval_min=%d\n",lneigh,theta,t0,tf,nval_min);
  // getchar();
  // printf("pft->t=%f\n pft->ft=%f\n",pft->t,pft->ft);

  // printf("After affectation of time:\n lneigh=%f\n theta=%f\n t0=%f\n tf=%f\n nval_min=%d\n",lneigh,theta,t0,tf,nval_min);
  ptmp = pft;
  t0 = TS_affect_t(t0, pft, BEGINNING_TS);
  tf = TS_affect_t(tf, pft, END_TS);
  // printf("ptmp->t=%f\n ptmp->ft=%f\n",ptmp->t,ptmp->ft);
  ptmp = TS_function_t_pointed(t0, ptmp, fp); // FB BL puts ptmp the closest to t0
  // printf("ptmp->t=%f\n ptmp->ft=%f\n",ptmp->t,ptmp->ft);
  // printf("t0=%f\n",t0);

  delta = theta;
  delta *= lneigh;

  pft_mv1 = NULL;
  pft_mv2 = NULL;
  t = ptmp->t;
  // while((t<tf)&&(ptmp!=NULL)){ //FB
  while ((t <= tf) && (ptmp != NULL)) { // FB <= instead of <
    // printf("t=%f\n",t);
    nval++;
    time_up = ptmp->t - delta;
    // printf("t=%f\n",t);
    // printf("time_up=%f\n",time_up);
    // getchar();

    pft_mv2 = TS_create_average_step(ptmp, lneigh, nval_min, time_up, t, fp);
    if (pft_mv2 != NULL)
      pft_mv1 = TS_secured_chain_fwd_ts(pft_mv1, pft_mv2);
    ptmp = ptmp->next;
    if (ptmp != NULL) { // FB Added control to avoid segmentation fault when the end of the series is reached
      t = ptmp->t;      // FB I think t should be moved to the next one
    }
  }

  return pft_mv1;
}

/**\fn s_ft *TS_min_ft(s_ft *pft)
 *\brief calculates the minimum of a time series
 *\return a pointer pft such that pft->ft is the minimum of the time series and pft->t is the corresponding time
 */
s_ft *TS_min_ft(s_ft *pft) {
  s_ft *min_ft;

  pft = TS_browse_ft(pft, BEGINNING_TS);
  min_ft = TS_create_function(0., BIG_TS);

  //  while(pft->next!=NULL){ //FB
  while (pft != NULL) { // FB Corrected, otherwise the last element of the time series is not considered in the computation of the minimum
    // printf("t=%f\n",pft->t);
    // getchar();
    if (fabs(pft->ft - CODE_TS) > EPS_TS && pft->ft < min_ft->ft) {
      min_ft->ft = pft->ft;
      min_ft->t = pft->t;
    }
    pft = pft->next;
  }

  return min_ft;
}

/**\fn s_ft *TS_max_ft(s_ft *pft)
 *\brief finds the maximum of a time series
 *\return a pointer pft such that pft->ft is the maximum of the time series and pft->t is the corresponding time
 */
s_ft *TS_max_ft(s_ft *pft) {
  s_ft *max_ft;

  pft = TS_browse_ft(pft, BEGINNING_TS);
  max_ft = TS_create_function(0., SMALL_TS);

  while (pft != NULL) {
    if (fabs(pft->ft - CODE_TS) > EPS_TS && pft->ft > max_ft->ft) {
      max_ft->ft = pft->ft;
      max_ft->t = pft->t;
    }
    pft = pft->next;
  }

  return max_ft;
}

/**\fn s_ft *TS_VCN(s_ft *pft,double lneigh, double theta, int nval_min, FILE *fp)
 *\brief Calculates the VCN-lneigh of the time series pft. theta and nval_min have the same meaning as in TS_moving_average: theta=0->downstream, theta=1 -> upstream, theta=0.5->fully centered; nval_min is the minimum number of values that have to be found in lneigh to calculate the moving average.
 * returns a pointer containing both the date of the minimum value and the minimum value over a lneigh-period of time
 *
 */
s_ft *TS_VCN(s_ft *pft, double lneigh, double theta, double t0, double tf, int nval_min, FILE *fp) {
  s_ft *pft_mv, *pVCN;

  pft_mv = TS_moving_average(pft, lneigh, theta, t0, tf, nval_min, fp);
  pVCN = TS_min_ft(pft_mv);
  // printf("t0=%f\n tf=%f\n t_VCN=%f\n VCN=%f\n",t0,tf,VCN->t,VCN->ft);
  // getchar();

  return pVCN;
}

/**\fn s_ft *TS_QMNA_X(s_ft *pft,double lneigh, double theta, int nval_min, double quantile,int yr0,int nclass,FILE *flog)
 *\brief Calculates the QMNA X of a ts (X=100/"quantile"). The QMNA5 is the 5yr return period-VCN of a time serie. Here we set the period of the VCN calculation to lneigh. theta and nval_min have the same meaning as in TS_moving_average: theta=0->downstream, theta=1 -> upstream, theta=0.5->fully centered; nval_min is the minimum number of values that have to be found (if=CODE_TS, set up to  in
 *lneigh to calculate the moving average. yr0 is the reference year for julian day calculation, if = CODE_TS, set up to INITIAL_YEAR_JULIAN_DAY_TS. nclass is the number of classes, if = CODE_TS, set up to NCLASS_TS returns the QMNA X value
 *
 */
double TS_qmna(s_ft *pft, double lneigh, double theta, int nval_min, double quantile, int yr0, int nclass, FILE *flog) {
  s_ft *pft_mv, *pVCN1, *pVCN2;
  s_ft **pfty;
  s_ft *pvcn;
  s_carac_ts *pcts;
  int i;
  double return_period;
  double nyr;
  double qmna;
  s_distrib_ts *pdistrib;
  double t0, tf;
  s_ft *pftmp;
#ifdef TS_DEBUG
  char name[STRING_LENGTH_LP];
  FILE *fpi;
#endif

  qmna = (double)CODE_TS;

  if (quantile == CODE_TS)
    quantile = QUANTILE_TS;

  return_period = 100 / quantile;
  pcts = TS_carac_ts(pft, yr0, flog);
  nyr = (double)pcts->nyr;

  if (nyr / return_period > NPERIOD_MIN_TS) {
    pfty = TS_cut_ts_annually(pft, yr0, flog);
    pVCN1 = pVCN2 = NULL;
    for (i = 0; i < pcts->nyr; i++) {
      pft_mv = NULL;
#ifdef TS_DEBUG
      sprintf(name, "yr%d.txt", i);
      fpi = fopen(name, "w");
      TS_print_ts_with_date(pfty[i], yr0, FR_TS, fpi, flog);
      fclose(fpi);
#endif
      pftmp = pfty[i];
      if (pftmp != NULL) {
        pftmp = TS_browse_ft(pftmp, BEGINNING_TS);
        t0 = pftmp->t;
        pftmp = TS_browse_ft(pftmp, END_TS);
        tf = pftmp->t;
        pft_mv = TS_moving_average(pfty[i], lneigh, theta, t0, tf, nval_min, flog);
      }
      if (pft_mv != NULL) {
        pVCN2 = TS_min_ft(pft_mv);
        pVCN1 = TS_secured_chain_fwd_ts(pVCN1, pVCN2);
        TS_free_ts(pft_mv, flog);
      } else
        LP_warning(flog, "In libts%4.2f %s line %d: No Moving average calculated for year %d and therefore no VCN. There might be an issue in the data or the algorithm; \n\tIgnored by libts%4.2f\n", VERSION_TS, __FILE__, __LINE__, i, VERSION_TS);
      TS_free_ts(pfty[i], flog);
    }
    pdistrib = create_distrib(pVCN1, nclass, flog);
    qmna = find_quantile(pdistrib, quantile);
    TS_free_distrib(pdistrib, flog);
    TS_free_ts(pVCN1, flog);
  } else
    LP_warning(flog, "In libts%4.2f %s %d : Impossible to calculate QMNA %4.2f with a %4.2f years long time series\n", VERSION_TS, __FILE__, __LINE__, return_period, nyr);
  TS_destroy_carac_ts(pcts); // NF 28/10/2014 to avoid memory leaks

  return qmna;
}

/**\fn double TS_weighted_average(s_ft *pftmp)
 *\brief calculates the weighted average of the time series pftmp (the weights are the intervals between the times of the series)
 *\return weighted average of the time series pftmp
 */
double TS_weighted_average(s_ft *pftmp) {
  double ltot, lloc;
  double x1, x2, x;
  double average = 0;
  s_ft *pft;

  pft = TS_browse_ft(pftmp, BEGINNING_TS);
  ltot = 0;

  if (pft->next == NULL) // FB these two lines added for the case when the time series is made of one value only
    average = pft->ft;   // FB
  else {
    while (pft->next != NULL) {
      x1 = pft->t;
      x2 = pft->next->t;
      x = (x1 + x2) / 2;
      lloc = x2 - x1;
      ltot += lloc;
      average += lloc * TS_interpol(pft->ft, x1, pft->next->ft, x2, x);
      pft = pft->next;
      // printf("x1=%f\n x2=%f\n x=%f\n lloc=%f\n",x1,x2,x,lloc);
      // printf("average=%f\n",average);
    }
    average /= ltot;
    // printf("ltot=%f\n average=%f\n",ltot,average);
    // getchar();
  }

  return average;
}
/**\fn double TS_average(s_ft *ptmp)
 *\brief calculate average of pft
 *\return average over the period pft
 */
double TS_average(s_ft *ptmp) {
  int nval = 0;
  double moyenne = 0.;
  s_ft *pft = NULL;

  // pft=ptmp;
  pft = TS_browse_ft(ptmp, BEGINNING_TS);

  // if(fabs(pft->ft-BIG_TS)>EPS_TS){
  if (fabs(pft->ft - BIG_TS) > EPS_TS && fabs(pft->ft - TS_INFINITE) > EPS_TS && fabs(pft->ft - CODE_TS) > EPS_TS) { // FB 13/06/16
    moyenne += pft->ft;
    nval++;
  }
  while (pft->next != NULL) {
    pft = pft->next;
    // if(fabs(pft->ft-BIG_TS)>EPS_TS) {
    if (fabs(pft->ft - BIG_TS) > EPS_TS && fabs(pft->ft - TS_INFINITE) > EPS_TS && fabs(pft->ft - CODE_TS) > EPS_TS) { // FB 13/06/16
      nval++;
      moyenne += pft->ft;
    }
  }
  if (nval == 0) {
    moyenne = INFINITE_DIV_TS;
    nval = 1;
  }
  moyenne /= nval;

  return moyenne;
}

/**\fn double TS_stdev(s_ft *ptmp,double moyenne)
 *\brief calculate std of pft
 *\return std over the period pft
 */
double TS_stdev(s_ft *ptmp, double moyenne) {
  int nval = 0;
  double ecart_type = 0.;
  s_ft *pft = NULL;

  // pft=ptmp;
  pft = TS_browse_ft(ptmp, BEGINNING_TS);

  if (fabs(pft->ft - CODE_TS) > EPS_TS && fabs(pft->ft - TS_INFINITE) > EPS_TS) { // FB 18/07/16
    nval++;
    ecart_type += (pft->ft - moyenne) * (pft->ft - moyenne);
  }

  while (pft->next != NULL) {
    pft = pft->next;
    if (fabs(pft->ft - CODE_TS) > EPS_TS && fabs(pft->ft - TS_INFINITE) > EPS_TS) { // FB 18/07/16
      nval++;
      ecart_type += (pft->ft - moyenne) * (pft->ft - moyenne);
    }
  }

  ecart_type /= nval;
  ecart_type = sqrt(ecart_type);

  return ecart_type;
}

/**\fn double *create_upper_class_limit(s_distrib *pdistrib)
 *\brief creates an array containing the values of the upper value (in range ie x-axis) of each class
 *\return the array ulim
 */
double *create_upper_class_limit(s_distrib_ts *pdistrib) {
  double *ulim;
  int i, nclass;
  double range;
  nclass = pdistrib->nclass;

  ulim = ((double *)calloc(nclass, sizeof(double)));
  for (i = 0; i < nclass; i++) {
    ulim[i] = pdistrib->min + (i + 1) * pdistrib->class_lag;
  }

  return ulim;
}

/**\fn double find_quantile(s_distrib_ts *pdistrib,double quantile)
 *\brief calculate the quantile value of quantile "quantile"
 *\return quantile value
 */
double find_quantile(s_distrib_ts *pdistrib, double quantile) {
  int count = 0, tmp = 0;
  ;
  int nval = 0;
  double quant, q_tmp;
  double v1, v2, c1, c2, valquant;
  int nclass, iclass = 0, icur = 0;
  double *ulim;
  int nval_class_cum;

  nval = pdistrib->nval;
  nclass = pdistrib->nclass;
  ulim = create_upper_class_limit(pdistrib);

  if (quantile < 100) // NF 7/9/2014 It cannot be nval if nval < 100. Otherwise libts is Bugged
  {
    quant = quantile;
    quant /= 100;
    q_tmp = 0;
    c1 = c2 = 0;
    q_tmp = 0;
    icur = iclass = 0;
    nval_class_cum = pdistrib->class_distrib[NVAL_CLASS][icur];
    while (c2 < quant) {
      c1 = c2;
      // v1=v2;
      v1 = pdistrib->raw_distrib[count];
      count++;
      if ((count + 1) > nval_class_cum) {
        icur = iclass;
        while (nval_class_cum < (count + 1)) {
          nval_class_cum += pdistrib->class_distrib[NVAL_CLASS][++iclass];
        }
        /*  while((icur+1)<iclass){
          count+=pdistrib->class_distrib[NVAL_CLASS][++icur];
        }
        //On a besoin alors de remettre à jour C1, v1
        c1=(double) (count-1);//car il faut decompter l'increment de un qui sert a verifier le changement de classe
        c1/=nval;
        v1=pdistrib->raw_distrib[count++];*/
      }

      // Calcul maintenant de c2 & v2
      if (count < nval) {
        v2 = pdistrib->raw_distrib[count];
        c2 = (double)count;
        c2 /= nval;
        if (v2 > ulim[icur]) {
          v2 = ulim[icur];
        }
        icur = iclass;
        //   if((iclass-icur)>2){v1=ulim[iclass-1]; }
      } else {
        c2 = 1.;
        v2 = pdistrib->max;
      }
    }
    valquant = TS_interpol(v1, c1, v2, c2, quant);
  } else {
    printf("libts.%4.2f %s line %d: Quantile %f does not exist or equals the max value.\n", VERSION_TS, __FILE__, __LINE__, quantile);
    // NF 7/9/2014 If quant=1, then valquant is the max value of the distrib.
    if (fabs(quant - 1) < EPS_TS)
      valquant = pdistrib->max;
    else
      valquant = CODE_TS;
  }

  free(ulim);
  return valquant;
}
/**\fn double correlation(s_ft *pftp1,s_ft *pftp2,double moyenne1, double moyenne2)
 *\brief Calcule le coefficient de corrélation de deux chroniques,
 * pft1 et pft2 doivent avoir le même nombre de valeurs et doivent être exprimées aux mêmes instants t
 *\arg pft1 : résultats de la simulation (modifié avec cree_chronique_comparaison), moyenne à recalculer avant coef_correlation
 *\arg pft2 : mesures pour validation, moyenne déjà calculée
 *\return correlation value
 */
double correlation(s_ft *pftp1, s_ft *pftp2, double moyenne1, double moyenne2) {
  double coef_corr, numerateur = 0., denom1 = 0., denom2 = 0.;
  s_ft *pft1, *pft2;
  pft1 = pftp1;
  pft2 = pftp2;

  pft1 = TS_browse_ft(pft1, BEGINNING_TS);
  pft2 = TS_browse_ft(pft2, BEGINNING_TS);

  numerateur += (pft1->ft - moyenne1) * (pft2->ft - moyenne2);
  denom1 += (pft1->ft - moyenne1) * (pft1->ft - moyenne1);
  denom2 += (pft2->ft - moyenne2) * (pft2->ft - moyenne2);

  while (pft1->next != NULL) {
    pft1 = pft1->next;
    pft2 = pft2->next;
    numerateur += (pft1->ft - moyenne1) * (pft2->ft - moyenne2);
    denom1 += (pft1->ft - moyenne1) * (pft1->ft - moyenne1);
    denom2 += (pft2->ft - moyenne2) * (pft2->ft - moyenne2);
  }

  coef_corr = numerateur / sqrt(denom1 * denom2);

  return coef_corr;
}

/**\fn void TS_linear_regression(s_ft *pftp1,s_ft *pftp2,double *a, double *b, FILE *fp)
 *\brief The function does the linear regression between two input time series *pftp1 and *pftp2, where the two time series are defined over the same time period. The function returns the coefficients a and b of the line y(x)=a+bx, where x is ft of pftp1 and y is ft of pftp2. It would have been simpler to code a function having (x,y) as input [instead of (t,x) and (t,y) as it is currently]. But I
 *did like this in order to be able to use existing functions of libts like TS_average.
 *
 *
 */
void TS_linear_regression(s_ft *pftp1, s_ft *pftp2, double *a, double *b, FILE *fp) {
  s_ft *pft1, *pft2;
  double mx2, mx, my, mxy, denom;
  int N;

  pft1 = pftp1;
  pft2 = pftp2;

  N = TS_length_ts(pft1);

  if (N != TS_length_ts(pft2))
    LP_error(fp, "The two time series have different length in function TS_linear_regression.");
  else if (N == 1) {
    *a = CODE_TS;
    *b = CODE_TS;
    LP_warning(fp, "Regression not possible for a series of one element only.\n");
  } else {
    pft1 = TS_browse_ft(pft1, BEGINNING_TS);
    pft2 = TS_browse_ft(pft2, BEGINNING_TS);

    mx2 = quadratic_mean(pft1);
    mx = TS_average(pft1);
    my = TS_average(pft2);
    mxy = TS_average_product(pft1, pft2);
    denom = mx2 - mx * mx;

    if (denom > EPS_TS) {
      *a = (mx2 * my - mx * mxy) / denom;
      *b = (mxy - mx * my) / denom;
    } else {
      *a = CODE_TS;
      *b = CODE_TS;
      LP_warning(fp, "Division by zero in TS_linear_regression.\n");
    }
  }
}

/**\fn double TS_average_product(s_ft *ptmp1,s_ft *ptmp2)
 *\brief This function computes the average of the product between two time series defined on the same period. Given ptmp1=(t1,f(t1);t2,f(t2);...) and ptmp2=(t1,g(t1);t2,g(t2);...), the function gives the average of the time series (t1,f(t1)*g(t1);t2,f(t2)*g(t2);...).
 *
 *
 */
double TS_average_product(s_ft *ptmp1, s_ft *ptmp2) {
  s_ft *pft1, *pft2;
  int nval = 0;
  double average_product = 0.;
  pft1 = ptmp1;
  pft2 = ptmp2;

  pft1 = TS_browse_ft(pft1, BEGINNING_TS);
  pft2 = TS_browse_ft(pft2, BEGINNING_TS);

  nval++;
  average_product += pft1->ft * pft2->ft;
  while (pft1->next != NULL && pft2->next != NULL) {
    pft1 = pft1->next;
    pft2 = pft2->next;
    nval++;
    average_product += pft1->ft * pft2->ft;
  }
  average_product /= nval;

  return average_product;
}

/**\fn double quadratic_mean(s_ft *ptmp)
 *\brief Calculate the quadratic mean of the time serie
 *
 *
 */
double quadratic_mean(s_ft *ptmp) {
  int nval = 0;
  double rmse = 0.;
  s_ft *pft;

  pft = ptmp;
  pft = TS_browse_ft(pft, BEGINNING_TS);

  nval++;
  rmse += pft->ft * pft->ft;
  while (pft->next != NULL) {
    pft = pft->next;
    nval++;
    rmse += pft->ft * pft->ft;
  }

  rmse /= nval;
  // rmse = sqrt(rmse);

  return rmse;
}

/**\fn s_ft *TS_create_average_step(s_ft *ptmp,double lneigh,int nval_min,double t0,double t,FILE *fp)
 *\brief calculates the average between t0 and t0+lneigh at time t; returns NULL if the number of values between t0 and t0+lneigh is less than nval_min.
 *
 */
s_ft *TS_create_average_step(s_ft *ptmp, double lneigh, int nval_min, double t0, double t, FILE *fp) {
  double tf;
  s_ft *pft, *pft_mv;
  s_ft *ppiece1;
  double val;
  int nval = 0;
  FILE *fpr, *fb;
  pft_mv = NULL;

  if (fabs(nval_min - CODE_TS) < EPS_TS)
    nval_min = NVAL_DT_MIN_TS;
  if (fabs(lneigh - CODE_TS) < EPS_TS)
    lneigh = TSTEP_STAT_TS;

  // printf("Dans create average step\n");
  tf = t0 + lneigh;
  ppiece1 = TS_extract_chro(ptmp, &nval, t0, tf, fp);
  if (ppiece1 != NULL) // FB Control added (it is needed at the beginning of the time series, where t=t0)
  {
    if (nval >= nval_min) {
      pft_mv = TS_create_function(t, TS_weighted_average(ppiece1));
    }
    ppiece1 = TS_browse_ft(ppiece1, BEGINNING_TS);
    TS_free_ts(ppiece1, fp);
  }
  // printf("nval=%d\n",nval);
  // getchar();
  return pft_mv;
}

/**\fn s_ft *TS_create_averaged_ts(s_ft *pft,double tstep,int nval_min,FILE *fp)
 *\brief calculate the averaged time series of a time series with a given time step (tstep).

 */
s_ft *TS_create_averaged_ts(s_ft *pft, double tstep, int nval_min, FILE *fp) {
  s_ft *ptmp;
  s_ft *pft_mv1, *pft_mv2;
  double time_up, t, tend;
  FILE *fb;

  if (fabs(nval_min - CODE_TS) < EPS_TS)
    nval_min = NVAL_DT_MIN_TS;
  if (fabs(tstep - CODE_TS) < EPS_TS)
    tstep = TSTEP_STAT_TS;

  pft_mv1 = NULL;
  pft_mv2 = NULL;

  ptmp = TS_browse_ft(pft, BEGINNING_TS);
  t = ptmp->t;
  // printf("t=%f\n",t);
  // getchar();
  tend = TS_affect_t(CODE_TS, ptmp, END_TS);
  // printf("tend=%f\n",tend);
  // getchar();
  while ((ptmp != NULL) && (t <= tend)) { // FB <= otherwise it does not compute the average for the last day
    t = floor(t);                         // FB floor function added (otherwise it does not take the time series between 00:00 and 00:00)
    time_up = t - CONV_SEC_DDEC_TS;
    ptmp = TS_function_t_pointed(time_up, ptmp, fp); // puts ptmp the closest to t
    pft_mv2 = TS_create_average_step(ptmp, tstep, nval_min, time_up, t, fp);

    if (pft_mv2 == NULL)
      pft_mv2 = TS_create_function(t, CODE_TS);
    pft_mv1 = TS_secured_chain_fwd_ts(pft_mv1, pft_mv2);

    t += tstep;
  }
  pft_mv1 = TS_browse_ft(pft_mv1, BEGINNING_TS);

  return pft_mv1;
}

/**\fn s_ft **TS_create_stat_step(s_ft *ptmp,double lneigh,int nval_min,double t0,double t,FILE *fp)
 *\brief calculates the statistics (weighted average, std) between t0 and t0+lneigh at time t; returns NULL if the number of values between t0 and t0+lneigh is less than nval_min.
 *
 */
s_ft **TS_create_stat_step(s_ft *ptmp, double lneigh, int nval_min, double t0, double t, FILE *fp) {
  double tf;
  s_ft *pft, **pft_mv;
  s_ft *ppiece1;
  double val;
  int nval = 0;
  FILE *fpr, *fb;
  int i;

  if (fabs(nval_min - CODE_TS) < EPS_TS)
    nval_min = NVAL_DT_MIN_TS;
  if (fabs(lneigh - CODE_TS) < EPS_TS)
    lneigh = TSTEP_STAT_TS;

  /*Allocation of pft_mv size*/
  pft_mv = (s_ft **)malloc(NSTAT_TS * sizeof(s_ft *));
  for (i = 0; i < NSTAT_TS; i++)
    pft_mv[i] = NULL;

  // printf("Dans create average step\n");
  tf = t0 + lneigh;
  if ((t >= t0) && (t <= tf)) {
    ppiece1 = TS_extract_chro(ptmp, &nval, t0, tf, fp);
    if (ppiece1 != NULL) // FB Control added (it is needed at the beginning of the time series, where t=t0)
    {
      if (nval >= nval_min) {
        pft_mv[AVERAGE_TS] = TS_create_function(t, TS_weighted_average(ppiece1));
        pft_mv[STD_TS] = TS_create_function(t, TS_stdev(ppiece1, pft_mv[AVERAGE_TS]->ft));
      }
      ppiece1 = TS_browse_ft(ppiece1, BEGINNING_TS);
      TS_free_ts(ppiece1, fp);
    }
  } else
    LP_warning(fp, "In libts%4.2f file %s l.%d t not in the extraction interval of TS_create_stat_step()\n", VERSION_TS, __FILE__, __LINE__);
  // printf("nval=%d\n",nval);
  // getchar();
  return pft_mv;
}

/**\fn s_ft **TS_create_stat_ts(s_ft *pft,double tstep,int nval_min,FILE *fp)
 *\brief calculate the simple statistics (weighted average,std) time series of a time series with a given time step (tstep). It is based on
TS_create_stat_step
 */
s_ft **TS_create_stat_ts(s_ft *pft, double tstep, int nval_min, FILE *fp) {
  s_ft *ptmp;
  s_ft **pft_mv1, **pft_mv2;
  double time_up, t, tend, t_value;
  FILE *fb;
  int i;

  if (fabs(nval_min - CODE_TS) < EPS_TS)
    nval_min = NVAL_DT_MIN_TS;
  if (fabs(tstep - CODE_TS) < EPS_TS)
    tstep = TSTEP_STAT_TS;

  /*Allocation of pft_mv size*/
  pft_mv1 = (s_ft **)malloc(NSTAT_TS * sizeof(s_ft *));
  for (i = 0; i < NSTAT_TS; i++)
    pft_mv1[i] = NULL;
  pft_mv2 = NULL;

  ptmp = TS_browse_ft(pft, BEGINNING_TS);
  t = ptmp->t;
  tend = TS_affect_t(CODE_TS, ptmp, END_TS);

  while ((ptmp != NULL) && (t <= tend)) {            // FB <= otherwise it does not compute the average for the last day
    time_up = floor(t);                              // FB floor function added (otherwise it does not take the time series between 00:00 and 00:00)
    ptmp = TS_function_t_pointed(time_up, ptmp, fp); // puts ptmp the closest to t
    t_value = time_up + tstep / 2;
    pft_mv2 = TS_create_stat_step(ptmp, tstep, nval_min, time_up, t, fp);

    for (i = 0; i < NSTAT_TS; i++) {
      if (pft_mv2[i] == NULL)
        pft_mv2[i] = TS_create_function(t_value, CODE_TS);
      pft_mv1[i] = TS_secured_chain_fwd_ts(pft_mv1[i], pft_mv2[i]);
    }
    t += tstep;
  }
  for (i = 0; i < NSTAT_TS; i++)
    pft_mv1[i] = TS_browse_ft(pft_mv1[i], BEGINNING_TS);

  return pft_mv1;
}

s_ft *TS_get_max_annual(s_ft *pft, int yr0, FILE *flog) {

  s_date_ts **pdate;
  s_date_ts *pd;
  s_ft *p1, *p2, *pchro;
  int yin, yend, yy, i;
  double t[NEXTREMA_TS];
  int nval = 5;

  p1 = p2 = NULL;
  pdate = TS_get_ts_border(pft, yr0, flog);
  yin = pdate[BEGINNING_TS]->yyyy;
  pdate[BEGINNING_TS]->mm = 1;
  pdate[BEGINNING_TS]->dd = 1;
  yend = pdate[END_TS]->yyyy;
  pdate[END_TS]->mm = 12;
  pdate[END_TS]->dd = 31;
  for (yy = yin; yy <= yend; yy++) {
    for (i = 0; i < NEXTREMA_TS; i++) {
      pdate[i]->yyyy = yy;
      t[i] = TS_date2julian_dd_hm(pdate[i], yr0, flog);
    }
    t[END_TS] += EPS2_TS;
    t[BEGINNING_TS] -= EPS2_TS;
    pchro = TS_extract_chro(pft, &nval, t[BEGINNING_TS], t[END_TS], flog);
    if (pchro != NULL) {
      p2 = TS_max_ft(pchro);
      TS_free_ts(pchro, flog);
      p1 = TS_secured_chain_fwd_ts(p1, p2);
    }
  }

  for (i = 0; i < NEXTREMA_TS; i++)
    free(pdate[i]);
  free(pdate);

  return p1;
}

double TS_sum_ts(s_ft *pft, FILE *flog) {
  s_ft *ptmp = NULL;
  double sum = 0.;

  if (pft == NULL)
    LP_error(flog, "In libts%4.2f %s at line %d: Time serie pointer is NULL.\n", VERSION_TS, __FILE__, __LINE__);

  ptmp = pft;
  while (ptmp != NULL) {
    sum += ptmp->ft;
    ptmp = ptmp->next;
  }
  return sum;
}
