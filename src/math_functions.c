/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libts
 * FILE NAME: math_functions.c
 *
 * CONTRIBUTORS: Nicolas FLIPO, Lauriane VILMIN, Shuaitao WANG,
 *               Baptiste LABARTHE, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: Management of time series including linear
 * interpolation for holes as well as convertions back and forth between
 * julian days and calendar dates.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libts Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <strings.h>
#include "libprint.h"
#include "param_ts.h"

/** \fn double TS_min(double a, double b)
 * \brief give minimum of two values
 */
double TS_min(double a, double b) {
  double mini;
  mini = a; // BL car si egaux la fonction renvoie un double non initialisé!!!
  mini = (a >= b) ? b : a;
  // printf("inside mini; mini = %f\n",mini);
  return mini;
}
/** \fn double  TS_max(double a, double b)
 * \brief give maximum of two values
 */
double TS_max(double a, double b) {
  double maxi;
  maxi = a; // BL car si egaux la fonction renvoie un double non initialisé!!!
  maxi = (a <= b) ? b : a;
  // printf("inside maxi; maxi = %f\n",maxi);
  return maxi;
}

/** \fn double interpol(double y1,double x1,double y2,double x2,double x)
 * \brief Fonction d'interpolation.
 *
 * La valeur d'une fonction étant connue entre deux points de corrdonnées
 * (x1,y1=f(x1)) et (x2,y2=f(x2)), on cherche la valeur de la fonction au point d'abscisse x.
 * \arg double y1 Valeur de la fonction au premier point
 * \arg double x1 Abscisse du premier point
 * \arg double y2 Valeur de la fonction au deuxième point
 * \arg double x2 Abscisse du deuxième point.
 * \arg double x Abscisse du point pour lequel on cherche la valeur de la fonction.
 * \return La valeur de la fonction f(x) au point d'abscisse x.
 */

double TS_interpol(double y1, double x1, double y2, double x2, double x) {
  if (fabs(x2 - x1) < 0.0000001)
    return ((y1 + y2) / 2.);
  else
    return (y1 + (x - x1) * (y2 - y1) / (x2 - x1));
}

/** \fn  double TS_avoid_inf(double x1,double x2)
 * \brief return 0 if division by 0
 */
double TS_avoid_inf(double x1, double x2) {
  x1 = (fabs(x2) > EPS_TS) ? (x1 / x2) : 0.;
  return x1;
}

/** \fn double TS_integrate(double t1,double f1,double t2,double f2)
 * \brief trapezoidal integration between two points (t1,f1) and (t2,f2)
 */
double TS_integrate(double t1, double f1, double t2, double f2) {
  double f;

  f = f1 + f2;
  f *= 0.5;
  f *= (t2 - t1);
  return f;
}

/** \fn int TS_is_value_closed_interval(double,double,double,FILE*)
 * \brief Checks if an input value belongs to the [thresh_min;thresh_max] closed interval
 * \return Return YES if so, NO otherwise.
 */
int TS_is_value_closed_interval(double input_value, double thresh_min, double thresh_max) {
  int answer = NO_TS;

  if (input_value >= thresh_min && input_value <= thresh_max)
    answer = YES_TS;

  return answer;
}
