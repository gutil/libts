/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libts
 * FILE NAME: print_ts.c
 *
 * CONTRIBUTORS: Nicolas FLIPO, Lauriane VILMIN, Shuaitao WANG,
 *               Baptiste LABARTHE, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: Management of time series including linear
 * interpolation for holes as well as convertions back and forth between
 * julian days and calendar dates.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libts Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <math.h>
#include <string.h>
#include "libprint.h"
#include "time_series.h"
/**\fn void TS_print_in_column_ft_ts(s_ft *pft,FILE *fp)
 *\brief print ft in file in a single column
 *
 */
void TS_print_in_column_ft_ts(s_ft *pft, FILE *fp) {
  s_ft *ptmp;
  ptmp = TS_browse_ft(pft, BEGINNING_TS);

  while (ptmp != NULL) {
    fprintf(fp, "%f\n", ptmp->ft);
    ptmp = ptmp->next;
  }
  fprintf(fp, "\n");
}
/**\fn void TS_print_in_line_ft_ts(s_ft *pft,FILE *fp)
 *\brief print ft in file in a single line
 *
 */
void TS_print_in_line_ft_ts(s_ft *pft, FILE *fp) {
  s_ft *ptmp;
  ptmp = TS_browse_ft(pft, BEGINNING_TS);

  while (ptmp != NULL) {
    fprintf(fp, ",%f", ptmp->ft);
    ptmp = ptmp->next;
  }
  fprintf(fp, "\n");
}
/**\fn void TS_print_ts(s_ft *pft,FILE *fp)
 *\brief print t ft in file (column)
 *
 */
void TS_print_ts(s_ft *pft, FILE *fp) {
  s_ft *ptmp;
  ptmp = TS_browse_ft(pft, BEGINNING_TS);

  while (ptmp != NULL) {
    LP_printf(fp, "%f %f\n", ptmp->t, ptmp->ft);
    ptmp = ptmp->next;
  }
}
/**\fn void TS_printf_ts(s_ft *pft,FILE *fp)
 *\brief print t ft in file without printing to the screen (column)
 *
 */
void TS_printf_ts(s_ft *pft, FILE *fp) {
  s_ft *ptmp;
  ptmp = TS_browse_ft(pft, BEGINNING_TS);

  while (ptmp != NULL) {
    fprintf(fp, "%f %f\n", ptmp->t, ptmp->ft);
    ptmp = ptmp->next;
  }
}
/**\fn void TS_print_ts_wsep(s_ft *pft,char sep,FILE *fp)
 *\brief print t ft in file (column) with a specified separator "sep"
 *
 */
void TS_print_ts_wsep(s_ft *pft, char sep, FILE *fp) {
  s_ft *ptmp;
  ptmp = TS_browse_ft(pft, BEGINNING_TS);

  while (ptmp != NULL) {
    LP_printf(fp, "%f%c%f\n", ptmp->t, sep, ptmp->ft);
    ptmp = ptmp->next;
  }
}
/**\fn void TS_print_raw_distrib(s_distrib_ts *pdistrib,FILE *fp)
 *\brief print raw_distrib
 *
 */
void TS_print_raw_distrib(s_distrib_ts *pdistrib, FILE *fp) {

  double *distrib;
  int nval;
  int i;

  distrib = pdistrib->raw_distrib;
  nval = pdistrib->nval;
  LP_printf(fp, "libts%4.2f: ++++ Printing a  raw distrib:\n", VERSION_TS);
  for (i = 0; i < nval; i++)
    LP_printf(fp, "%d %f\n", i, distrib[i]);
  //  LP_printf(fp,"libts%3.1f: Raw distrib printed ++++\n",VERSION_TS);
}
/**\fn void TS_print_carac_distrib(s_distrib_ts *pdistrib,FILE *fp)
 *\brief print carac_distrib
 *
 *print for each classes the min max and than all the values
 */
void TS_print_carac_distrib(s_distrib_ts *pdistrib, FILE *fp) {
  int nclass;
  double clag;
  int i, j;
  double mini, maxi;

  nclass = pdistrib->nclass;
  clag = pdistrib->class_lag;

  LP_printf(fp, "#libts%4.2f: ++++ Printing a  distrib of %d values, %d classes of lag = %f with Min = %f and Max=%f\n", VERSION_TS, pdistrib->nval, nclass, clag, pdistrib->min, pdistrib->max);

  LP_printf(fp, "Class,Min,Max");

  mini = pdistrib->min;
  maxi = mini;
  maxi += clag;

  for (j = 0; j < NCOL_CLASS; j++)
    LP_printf(fp, ",%s", TS_name_order_distrib(j, fp));
  LP_printf(fp, "\n");
  for (i = 0; i < nclass; i++) {
    LP_printf(fp, "%d,%9.5f,%9.5f", i, mini, maxi);
    mini += clag;
    maxi += clag;
    for (j = 0; j < NCOL_CLASS; j++)
      LP_printf(fp, ",%6.3f", pdistrib->class_distrib[j][i]);
    LP_printf(fp, "\n");
  }
  // LP_printf(fp,"libts%3.1f: Raw %d classes-distrib printed ++++\n",VERSION_TS,nclass);
}
/**\fn void TS_print_header_quantiles(int *nq,FILE *fp,double *quant)
 *\brief print quantiles header
 *
 */
void TS_print_header_quantiles(int *nq, FILE *fp, double *quant) {
  int i;

#ifdef TS_DEBUG
  LP_printf(fp, "#libts%4.2f TS_print_quantiles : %d quantiles\n", VERSION_TS, *nq);
#endif
  if (*nq == NQUANT_TS) {
    for (i = 0; i < *nq; i++)
      LP_printf(fp, "%s ", TS_name_quantile(i, fp));
  } else {
    for (i = 0; i < *nq; i++)
      LP_printf(fp, "Q%4.2f ", quant[i]);
  }
  LP_printf(fp, "\n");
}
/**\fn void TS_print_quantiles(int *nq,double *quant,FILE *fp)
 *\brief print quantiles in file
 *
 */
void TS_print_quantiles(int *nq, double *quant, FILE *fp) {
  int i;

  for (i = 0; i < *nq; i++)
    LP_printf(fp, "%f ", quant[i]);
  LP_printf(fp, "\n");
}
/**\fn void TS_print_all_stats_ts(s_distrib_ts *pdistrib,int *nq,FILE *fp)
 *\brief print averages, std then quantiles in file
 *
 */
void TS_print_all_stats_ts(s_distrib_ts *pdistrib, int *nq, FILE *fp) {
  int i;

  LP_printf(fp, " %16.10f", pdistrib->average);
  LP_printf(fp, " %16.10f", pdistrib->std);
  for (i = 0; i < *nq; i++)
    LP_printf(fp, " %16.10f", pdistrib->quant[i]);
  LP_printf(fp, "\n");
}

/**\fn void TS_print_ts_with_date(s_ft *pft,int yr0,int icode FILE *fp,FILE *flog)
 *\brief print a time series in file (column) with with a specified separator "sep" and date
 * in the following format jj/mm/yyyy hh:mm:ss val
 * If yr0=CODE_TS, then yr0 will be set to INITIAL_YEAR_JULIAN_DAY_TS by TS_convert_julian2date()
 * icode is an encoding code for the date CODE_TS,FR_TS or US_TS
 * *fp is the file where the time series will be printed
 * *flog is the log file
 */
void TS_print_ts_with_date(s_ft *pft, int yr0, int icode, FILE *fp, FILE *flog) {
  s_date_ts *pd;
  s_ft *pftmp;

  if (pft != NULL) {
    pftmp = TS_browse_ft(pft, BEGINNING_TS);
    while (pftmp != NULL) {
      pd = TS_convert_julian2date_decimal(pftmp->t, yr0, flog);
      TS_print_date(pd, icode, fp, flog);
      fprintf(fp, " ");
      TS_print_time(pd, fp, flog);
      fprintf(fp, " ");
      fprintf(fp, " %f\n", pftmp->ft);
      pftmp = pftmp->next;
    }
  } else
    LP_warning(flog, "In libts%4.2f %s in TS_print_ts_with_date() line %d: pft is NULL\n", VERSION_TS, __FILE__, __LINE__);
}

/**\fn void TS_print_ts_with_date_only(s_ft *pft,int yr0,int icode FILE *fp,FILE *flog)
 *\brief print a time series in file (column) with with a specified separator "sep" and date
 * in the following format jj/mm/yyyy val
 * If yr0=CODE_TS, then yr0 will be set to INITIAL_YEAR_JULIAN_DAY_TS by TS_convert_julian2date()
 * icode is an encoding code for the date CODE_TS,FR_TS or US_TS
 * *fp is the file where the time series will be printed
 * *flog is the log file
 */
void TS_print_ts_with_date_only(s_ft *pft, int yr0, int icode, FILE *fp, FILE *flog) {
  s_date_ts *pd;
  s_ft *pftmp;

  if (pft != NULL) {
    pftmp = TS_browse_ft(pft, BEGINNING_TS);
    while (pftmp != NULL) {
      pd = TS_convert_julian2date_decimal(pftmp->t, yr0, flog);
      TS_print_date(pd, icode, fp, flog);
      fprintf(fp, " ");
      fprintf(fp, " %f\n", pftmp->ft);
      pftmp = pftmp->next;
    }
  } else
    LP_warning(flog, "In libts%4.2f %s in TS_print_ts_with_date() line %d: pft is NULL\n", VERSION_TS, __FILE__, __LINE__);
}

/**\fn void TS_print_ts_with_date_wsep(s_ft *pft,int yr0,int icode,char sep, FILE *fp,FILE *flog)
 *\brief a time series in the following format jj/mm/yyyy<sep>hh:mm:ss<sep>val
 * If yr0=CODE_TS, then yr0 will be set to INITIAL_YEAR_JULIAN_DAY_TS by TS_convert_julian2date()
 * icode is an encoding code for the date CODE_TS,FR_TS or US_TS
 * *fp is the file where the time series will be printed
 * *flog is the log file
 */
void TS_print_ts_with_date_wsep(s_ft *pft, int yr0, int icode, char sep, FILE *fp, FILE *flog) {
  s_date_ts *pd;
  s_ft *pftmp;

  if (pft != NULL) {
    pftmp = TS_browse_ft(pft, BEGINNING_TS);
    while (pftmp != NULL) {
      pd = TS_convert_julian2date_decimal(pftmp->t, yr0, flog);
      TS_print_date(pd, icode, fp, flog);
      fprintf(fp, " ");
      TS_print_time(pd, fp, flog);
      fprintf(fp, "%c", sep);
      fprintf(fp, "%f\n", pftmp->ft);
      pftmp = pftmp->next;
    }
  } else
    LP_warning(flog, "In libts%4.2f %s in TS_print_ts_with_date() line %d: pft is NULL\n", VERSION_TS, __FILE__, __LINE__);
}

/**\fn void TS_print_ts_with_date_wsep_hh_mm(s_ft *pft,int yr0,int icode,char sep, FILE *fp,FILE *flog)
 *\brief a time series in the following format jj/mm/yyyy<sep>hh:mm<sep>val
 * If yr0=CODE_TS, then yr0 will be set to INITIAL_YEAR_JULIAN_DAY_TS by TS_convert_julian2date()
 * icode is an encoding code for the date CODE_TS,FR_TS or US_TS
 * *fp is the file where the time series will be printed
 * *flog is the log file
 */
void TS_print_ts_with_date_wsep_hh_mm(s_ft *pft, int yr0, int icode, char sep, FILE *fp, FILE *flog) {
  s_date_ts *pd;
  s_ft *pftmp;

  if (pft != NULL) {
    pftmp = TS_browse_ft(pft, BEGINNING_TS);
    while (pftmp != NULL) {
      pd = TS_convert_julian2date_decimal(pftmp->t, yr0, flog);
      TS_print_date(pd, icode, fp, flog);
      fprintf(fp, " ");
      TS_print_time_hh_mm(pd, fp, flog);
      fprintf(fp, "%c", sep);
      fprintf(fp, "%f\n", pftmp->ft);
      pftmp = pftmp->next;
    }
  } else
    LP_warning(flog, "In libts%4.2f %s in TS_print_ts_with_date() line %d: pft is NULL\n", VERSION_TS, __FILE__, __LINE__);
}
