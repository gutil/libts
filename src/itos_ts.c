/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libts
 * FILE NAME: itos_ts.c
 *
 * CONTRIBUTORS: Nicolas FLIPO, Lauriane VILMIN, Shuaitao WANG,
 *               Baptiste LABARTHE, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: Management of time series including linear
 * interpolation for holes as well as convertions back and forth between
 * julian days and calendar dates.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libts Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "time_series.h"
#include "libprint.h"

char *TS_name_quantile(int answer, FILE *fp) {
  char *name;

  switch (answer) {
  case Q2: {
    name = strdup("Q2");
    break;
  }
  case Q5: {
    name = strdup("Q5");
    break;
  }
  case Q10: {
    name = strdup("Q10");
    break;
  }
  case Q25: {
    name = strdup("Q25");
    break;
  }
  case Q50: {
    name = strdup("Q50");
    break;
  }
  case Q75: {
    name = strdup("Q75");
    break;
  }
  case Q90: {
    name = strdup("Q90");
    break;
  }
  case Q95: {
    name = strdup("Q95");
    break;
  }
  case Q98: {
    name = strdup("Q98");
    break;
  }
  default: {
    LP_error(fp, "DEFAULT, unknown parameter %d in itos_ts.c TS_name_quantile()\n", answer);
    break;
  }
  }

  return name;
}

char *TS_name_order_distrib(int field, FILE *fp) {
  char *name;

  switch (field) {
  case NVAL_CLASS: {
    name = strdup("N");
    break;
  }
  case FREQ_CLASS: {
    name = strdup("percent");
    break;
  }
  case CUMUL_CLASS: {
    name = strdup("Ntot");
    break;
  }
  case FREQ_CUMUL: {
    name = strdup("percent_tot");
    break;
  }
  default: {
    LP_error(fp, "DEFAULT, unknown parameter %d in itos_ts.c TS_name_order_distrib()\n", field);
    break;
  }
  }

  return name;
}

char *TS_name_month(int month, FILE *fp) {
  char *name;

  switch (month) {
  case JANUARY: {
    name = strdup("JAN");
    break;
  }
  case FEBRUARY: {
    name = strdup("FEB");
    break;
  }
  case MARCH: {
    name = strdup("MAR");
    break;
  }
  case APRIL: {
    name = strdup("APR");
    break;
  }
  case MAY: {
    name = strdup("MAY");
    break;
  }
  case JUNE: {
    name = strdup("JUN");
    break;
  }
  case JULY: {
    name = strdup("JUL");
    break;
  }
  case AUGUST: {
    name = strdup("AUG");
    break;
  }
  case SEPTEMBER: {
    name = strdup("SEP");
    break;
  }
  case OCTOBER: {
    name = strdup("OCT");
    break;
  }
  case NOVEMBER: {
    name = strdup("NOV");
    break;
  }
  case DECEMBER: {
    name = strdup("DEC");
    break;
  }
  default: {
    LP_error(fp, "DEFAULT, unknown  month %d in itos_ts.c TS_name_month()\n", month);
    break;
  }
  }

  return name;
}

char *TS_name_bissex(int code) {

  char *name;

  switch (code) {
  case NORMAL_TS:
    name = strdup("NORMAL");
    break;
  case CENTURY_TS:
    name = strdup("CENTURY");
    break;
  case FOURCENTURY_TS:
    name = strdup("400YR");
    break;
  }

  return name;
}

char *TS_name_time_scale(int code, FILE *fp) {

  char *name;

  switch (code) {
  case ANNUAL_TS:
    name = strdup("ANNUAL");
    break;
  case MONTHLY_TS:
    name = strdup("MONTHLY");
    break;
  case DAILY_TS:
    name = strdup("DAILY");
    break;
  case VCN_TS:
    name = strdup("VCN");
    break;
  case QMNA_TS:
    name = strdup("QMNA");
    break;
  default: {
    LP_error(fp, "In libts%4.2f DEFAULT, unknown  time scale in %s l.%d\n", VERSION_TS, __FILE__, __LINE__);
    break;
  }
  }

  return name;
}

char *TS_name_date_encoding(int icode, FILE *fp) {

  char *name;

  switch (icode) {
  case FR_TS:
    name = strdup("FR");
    break;
  case US_TS:
    name = strdup("US");
    break;
  default: {
    LP_error(fp, "In libts%4.2f %s line %d: unknown endoding date\n", VERSION_TS, __FILE__, __LINE__);
    break;
  }
  }

  return name;
}

char *TS_name_stat(int icode, FILE *fp) {

  char *name;

  switch (icode) {
  case AVERAGE_TS:
    name = strdup("AVERAGE");
    break;
  case STD_TS:
    name = strdup("STD");
    break;
  default: {
    LP_error(fp, "In libts%4.2f %s line %d: unknown endoding date\n", VERSION_TS, __FILE__, __LINE__);
    break;
  }
  }

  return name;
}
