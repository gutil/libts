#-------------------------------------------------------------------------------
# 
# LIBRARY NAME: libts
# FILE NAME: update_version.sh
# 
# CONTRIBUTORS: Nicolas FLIPO, Lauriane VILMIN, Shuaitao WANG, 
#               Baptiste LABARTHE, Nicolas GALLOIS
# 
# LIBRARY BRIEF DESCRIPTION: Management of time series including linear 
# interpolation for holes as well as convertions back and forth between 
# julian days and calendar dates.
#
# Library developed at the Geosciences Center, joint research center 
# of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
#
# COPYRIGHT: (c) 2022 Contributors to the libts Library.
# CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
#          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
#
# All rights reserved. This Library and the accompanying materials
# are made available under the terms of the Eclipse Public License v2.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v20.html
# 
#------------------------------------------------------------------------------*/


#Automatic change of the version number
#> update_version.sh <new_version_number>

#gawk -f update_makefile.awk -v nversion=$1 Makefile
#mv awk.out Makefile

gawk -f update_param.awk -v nversion=$1 param_ts.h
mv awk.out param_ts.h

#gawk -f update_doxygen.awk -v nversion=$1 test_doxygen
#mv awk.out test_doxygen

echo "Version number updated in param.h and doxygen cmd file to" $1
