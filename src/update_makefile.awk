#-------------------------------------------------------------------------------
# 
# LIBRARY NAME: libts
# FILE NAME: update_makefile.awk
# 
# CONTRIBUTORS: Nicolas FLIPO, Lauriane VILMIN, Shuaitao WANG, 
#               Baptiste LABARTHE, Nicolas GALLOIS
# 
# LIBRARY BRIEF DESCRIPTION: Management of time series including linear 
# interpolation for holes as well as convertions back and forth between 
# julian days and calendar dates.
#
# Library developed at the Geosciences Center, joint research center 
# of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
#
# COPYRIGHT: (c) 2022 Contributors to the libts Library.
# CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
#          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
#
# All rights reserved. This Library and the accompanying materials
# are made available under the terms of the Eclipse Public License v2.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v20.html
# 
#------------------------------------------------------------------------------*/


BEGIN{
  name="awk.out";
}
{
  if (NR==39)
    printf("V_TS=%4.2f\n",nversion)>name;
  else
    printf("%s\n",$0)>name;
}
