/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libts
 * FILE NAME: cmp_kro.c
 *
 * CONTRIBUTORS: Nicolas FLIPO, Lauriane VILMIN, Shuaitao WANG,
 *               Baptiste LABARTHE, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: Management of time series including linear
 * interpolation for holes as well as convertions back and forth between
 * julian days and calendar dates.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libts Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <math.h>
#include <strings.h>
#include "libprint.h"
#include "time_series.h"

/**\fn double TS_interpolate_ts(double t,s_ft *pft)
 *\brief Permet de trouver la valeur de la chronique à un instant t donné
 *
 */
double TS_interpolate_ts(double t, s_ft *pft) {
  double inter;
  s_ft *ptmp = NULL;

  while ((pft != NULL) && (t > pft->t)) {
    ptmp = pft;
    pft = pft->next;
  }
  if ((pft != NULL) && (ptmp != NULL)) {
    if ((fabs(pft->t - t) > EPS_TS) && (fabs(ptmp->t - t) > EPS_TS)) {
      inter = TS_interpol(ptmp->ft, ptmp->t, pft->ft, pft->t, t);
      pft = ptmp; // to avoid seg fault if next t is included in the same time step...
    }
    // In case t is already part of the ts pft
    else {
      if (fabs(pft->t - t) < EPS_TS)
        inter = pft->ft;
      else {
        inter = ptmp->ft;
        pft = ptmp;
      }
    }
  } else {
    if (ptmp == NULL)
      inter = pft->ft;
    else {
      inter = ptmp->ft;
      pft = ptmp; // to avoid seg fault
    }
  }

  return inter;
}

/**\fn s_ft *TS_create_simultaneous_ts(s_ft *pft1,s_ft *pft2)
 *\brief permet de créer deux série temporelles simultanées
 *
 *Pour comparer pft1 à pft2 par la suite, crée à partir de pft1 par interpolation une nouvelle chronique de même longueur que pft2,
 *décrite aux mêmes instants t que pft2
 *\arg pft1 série temporelle des résultats de simul
 *\arg pft2 série temporelle pour validation
 */

s_ft *TS_create_simultaneous_ts(s_ft *pft1, s_ft *pft2) {
  s_ft *pft3;

  pft3 = TS_init_ft();
  pft3->t = pft2->t;
  pft3->ft = TS_interpolate_ts(pft2->t, pft1);

  while (pft2->next != NULL) {
    pft2 = pft2->next;
    pft3->next = TS_init_ft();
    pft3->next->prev = pft3;
    pft3 = pft3->next;
    pft3->t = pft2->t;
    pft3->ft = TS_interpolate_ts(pft2->t, pft1);
  }

  pft1 = TS_browse_ft(pft1, BEGINNING_TS);
  pft2 = TS_browse_ft(pft2, BEGINNING_TS);
  pft3 = TS_browse_ft(pft3, BEGINNING_TS);

  return pft3;
}

//
// pfts : résultats de simulation
// pfto : mesures pour validation
/**\fn void cmp_kro(s_ft *pfts,s_ft *pfto,FILE *fp)
 * \brief Function to print statistic in files
 *\arg pfts : simulated data
 *\arg pfto : observed data
 *\arg fp : output file
 *\return print rmse bias correlation and rbias in fp file
 */
void TS_cmp_kro(s_ft *pfts, s_ft *pfto, FILE *fp) {
  s_ft *pft[NKRO_TS];
  s_ft *pftdiff; // ts of differences between simulations and observation at the time t of observations.
  double mean[NKRO_TS];
  int i;
  double corr, absbias, rbias, rmse, quadmean;
  ;

  pft[OBS_TS] = pfto;
  pft[SIM_TS] = TS_create_simultaneous_ts(pfts, pfto);
  for (i = 0; i < NKRO_TS; i++) {
    mean[i] = TS_average(pft[i]);
  }
  corr = correlation(pft[SIM_TS], pft[OBS_TS], mean[SIM_TS], mean[OBS_TS]);
  pftdiff = TS_substract_fts(pft[SIM_TS], pft[OBS_TS]);

  absbias = TS_average(pftdiff);
  rbias = absbias / mean[OBS_TS];
  quadmean = quadratic_mean(pftdiff);
  rmse = sqrt(quadmean);

  if (fp != NULL) {
    LP_printf(fp, "rmse,bias,rbias,correlation\n");
    LP_printf(fp, "%f,%f,%f,%f\n", rmse, absbias, rbias, corr);
  }
}
