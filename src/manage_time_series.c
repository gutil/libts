/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libts
 * FILE NAME: manage_time_series.c
 *
 * CONTRIBUTORS: Nicolas FLIPO, Lauriane VILMIN, Shuaitao WANG,
 *               Baptiste LABARTHE, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: Management of time series including linear
 * interpolation for holes as well as convertions back and forth between
 * julian days and calendar dates.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libts Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include "time_series.h"

/** \fn s_ft *TS_init_ft()
 *\brief creates (allocates and initializes to NULL) a s_ft
 * \return the pointer towards pft
 */
s_ft *TS_init_ft() {
  s_ft *pft;
  pft = new_function();
  bzero((char *)pft, sizeof(s_ft));

  return pft;
}
/** \fn s_ft *TS_browse_ft(s_ft *ptmp,int deb_fin)
 *\brief Function used to browse a time series
 * \return the pointer towards the beginning or the end of the series
 */
s_ft *TS_browse_ft(s_ft *ptmp, int deb_fin) {
  s_ft *pft;
  pft = NULL;
  pft = ptmp;

  if (pft != NULL) {
    if (deb_fin == BEGINNING_TS) {
      while (pft->prev != NULL) {
        pft = pft->prev;
      }
    }

    if (deb_fin == END_TS) {
      while (pft->next != NULL) {
        pft = pft->next;
      }
    }
  } else {
    fprintf(stderr, "In libts%4.2f %s l.%d ->The input pointer of browse_ft is Null! Ignored by libts, but may create extra problem further in the code\n", VERSION_TS, __FILE__, __LINE__);
  }
  return pft;
}

/**\fn s_ft *ins_pointer_t(s_ft *pft,s_ft *pft_prev,int loc)
 *\brief Inserts a pointer in a chain of temporal pointers
 *at the given location (BEGINNING_TS, END_TS, MIDDLE)
 * \return The returned pointer points towards the inserted space
 */
s_ft *ins_pointer_t(s_ft *pft, s_ft *pft_prev, int loc) {
  s_ft *pftempo;

  pftempo = TS_init_ft();

  switch (loc) {
  case BEGINNING_TS: {
    pft->prev = pftempo;
    pftempo->next = pft;
    pft = pftempo;
    break;
  }
  case END_TS: {
    pftempo->prev = pft;
    pft->next = pftempo;
    pft = pftempo;
    break;
  }
  case MIDDLE_TS: {
    pftempo->next = pft;
    pftempo->prev = pft_prev;
    pft_prev->next = pftempo;
    pft->prev = pftempo;
    pft = pftempo;
    break;
  }
  }

  return pft;
}

/**\fn s_ft *TS_compare_time(s_ft *pft,double checked_time)
 *\brief Checks if the time t belongs to the series, otherwise creats a new temporal pointer with ins_pointer_t()
 *
 */
s_ft *TS_compare_time(s_ft *pft, double checked_time) {
  s_ft *pftempo;
  int i = CODE_TS;

  // pftempo = TS_init_ft();

  pft = TS_browse_ft(pft, BEGINNING_TS);
  pftempo = pft;

  while (pftempo != NULL) {
    if (pftempo->t == checked_time)
      i = 0;
    pftempo = pftempo->next;
  }

  if (i == CODE_TS) {
    pftempo = pft;

    if ((pftempo->prev == NULL) && (checked_time < pftempo->t)) {
      /* case of a single value */
      pft = ins_pointer_t(pft, pft, BEGINNING_TS);
      pft->ft = pft->next->ft;
    } else {
      if ((pftempo->next == NULL) && (checked_time > pftempo->t)) {
        pft = ins_pointer_t(pft, pft, END_TS);
        pft->ft = pft->prev->ft;
      }

      else {
        while (pftempo != NULL) {
          if (pftempo->next == NULL) {
            if (checked_time > pftempo->t)
              pft = pftempo;
          } else {
            if (checked_time > pftempo->t)
              pft = pftempo;
          }
          pftempo = pftempo->next;
        }
        if (pft->next != NULL) {
          pft = ins_pointer_t(pft->next, pft, MIDDLE_TS);
          pft->ft = TS_interpol(pft->prev->ft, pft->prev->t, pft->next->ft, pft->next->t, checked_time);
        } else {
          pft = ins_pointer_t(pft, pft, END_TS);
          pft->ft = pft->prev->ft;
        }
      }
    }
    pft->t = checked_time;
  }
  pft = TS_browse_ft(pft, BEGINNING_TS);

  return pft;
}

/**\fn s_ft *TS_create_function(double t,double q)
 *\brief Creates a new ft value
 *
 * one single value, included in a time series
 * \return the address of the new value
 */
s_ft *TS_create_function(double t, double q) {
  s_ft *pft;

  pft = TS_init_ft();
  pft->t = t;
  pft->ft = q;
  return pft;
}

/**\fn double TS_function_value_t(double t0,s_ft *ptmp,FILE *fp)
 * \brief Calculates the value of a meteorological variable at the current time
 * with the given discrete values
 */
double TS_function_value_t(double t0, s_ft *ptmp, FILE *fp)

{
  s_ft *pft2, *pft;
  double ft;

  pft = ptmp;

  if (pft == NULL) {
    LP_error(fp, "libts %4.2f in TS_function_value_t() --> No time series sent to the routine\n", VERSION_TS);
  }

  if (t0 > pft->t) {

    if (pft->prev != NULL)
      pft2 = pft->prev;
    else
      pft2 = pft;
    while ((pft2->next != NULL) && (pft2->t < t0))
      pft2 = pft2->next;
    // if (pft2->prev != NULL) //SW si pft2->next == NULL et pft2->t < t0 ca va poser un probleme
    // if (pft2->prev != NULL && (pft->t > t0)) // SW 2018/01/05 erreur
    if (pft2->prev != NULL && (pft2->t > t0))
      ft = TS_interpol(pft2->prev->ft, pft2->prev->t, pft2->ft, pft2->t, t0);
    else
      ft = pft2->ft;

  }

  else {
    if (pft->next != NULL)
      pft2 = pft->next;
    else
      pft2 = pft;
    while ((pft2->prev != NULL) && (pft2->t > t0))
      pft2 = pft2->prev;
    // if (pft2->next != NULL)//SW si pft2->next == NULL et pft2->t > t0 ca va poser un probleme
    // if (pft2->next != NULL && (pft->t < t0)) // SW 2018/01/05 erreur
    if (pft2->next != NULL && (pft2->t < t0))
      ft = TS_interpol(pft2->next->ft, pft2->next->t, pft2->ft, pft2->t, t0);
    else
      ft = pft2->ft;
  }

  return ft;
}

/*double TS_function_value_t(double t0,s_ft *ptmp,FILE *fp) // SW 01/03/2017

{
  s_ft *pft;
  double ft;

  pft=ptmp;
  pft = TS_browse_ft(pft,BEGINNING_TS);
  if (pft == NULL) {
    LP_error(fp,"libts %4.2f in TS_function_value_t() --> No time series sent to the routine\n",VERSION_TS);
  }


  if(t0>pft->t){
  while ((pft->next != NULL) && (pft->t < t0))
          pft = pft->next;
  if ((pft->prev != NULL) && (pft->t > t0)) //SW 02/03/2017
    ft = TS_interpol(pft->prev->ft,pft->prev->t,pft->ft,pft->t,t0);
  else
    ft = pft->ft;
  }
  else
        ft = pft->ft;

  pft = NULL;

  return ft;
}*/

/** \fn s_ft *TS_chain_fwd_ts(s_ft *pd1,s_ft *pd2)
/* \brief Links two elements with structure s_ft */
s_ft *chain_fwd_ts(s_ft *pd1, s_ft *pd2) {
  pd1->next = pd2;
  pd2->prev = pd1;
  return pd2;
}

/** \fn s_ft *TS_chain_bck_ts(s_ft *pd1,s_ft *pd2)
 * \brief Links two elements with structure s_ft bck, ie usefull for yacc for undetermined list
 *
 * Read the pile from top (end of the list) to bottom (beginning of the list), but return a pointer to the beginning of the list as a first element
 */
s_ft *TS_chain_bck_ts(s_ft *pd1, s_ft *pd2) {
  pd1->next = pd2;
  pd2->prev = pd1;
  return pd1;
}

/**\fn s_ft *chain_functions(s_ft *pd1,s_ft *pd2)
 *\brief same function than s_ft *TS_chain_bck_ts()
 */
s_ft *chain_functions(s_ft *pd1, s_ft *pd2) {
  pd1->next = pd2;
  pd2->prev = pd1;
  return pd1;
}
/**\fn s_ft *TS_secured_chain_fwd_ts(s_ft *pft1,s_ft *pft2)
 *\brief chain ft with the next one
 */
s_ft *TS_secured_chain_fwd_ts(s_ft *pft1, s_ft *pft2) {
  if (pft1 != NULL)
    pft1 = chain_fwd_ts(pft1, pft2);
  else
    pft1 = pft2;
  return pft1;
}
/**\fn s_ft *TS_secured_chain_bck_ts(s_ft *pft1,s_ft *pft2)
 *\brief chain ft with the next one
 */
s_ft *TS_secured_chain_bck_ts(s_ft *pft1, s_ft *pft2) {
  if (pft1 != NULL)
    pft1 = TS_chain_bck_ts(pft1, pft2);
  else
    pft1 = pft2;
  return pft1;
}
/**\fn s_ft *TS_add_pointers(s_ft *pftadd,s_ft *pft1,s_ft *pft2)
 *\brief function to add two ft from pft1, and pft2 together in a third pointer ft field in pft_add
 *
 *The function verifies the date compatibility
 *\return pftadd
 *
 */
s_ft *TS_add_pointers(s_ft *pftadd, s_ft *pft1, s_ft *pft2) {

  if (fabs(pft1->t - pft2->t) < EPS_TS) {
    pftadd->t = pft1->t;
    pftadd->ft = pft1->ft + pft2->ft;
  } else {
    LP_error(stderr, "In libts%4.2f %s l%d, Trying to add to pointers with different dates", VERSION_TS, __FILE__, __LINE__);
  }
  return pftadd;
}

// Attention maintenant il faut que pft1 et pft2 soient exprimées au mêmes instants
/**\fn s_ft *TS_add_functions(s_ft *pftmp1,s_ft *pftmp2)
 *\brief function to add two ft together
 *
 *Warning : pft1 and pft2 have to be define at same t
 *\return pft=pft1+pft2
 *
 */
s_ft *TS_add_functions(s_ft *pftmp1, s_ft *pftmp2) {
  s_ft *pft_new;
  s_ft *pft1, *pft2;

  pft1 = TS_browse_ft(pftmp1, BEGINNING_TS);
  pft2 = TS_browse_ft(pftmp2, BEGINNING_TS);

  pft_new = TS_init_ft();
  pft_new = TS_add_pointers(pft_new, pft1, pft2);

  while (pft1->next != NULL) {
    pft1 = pft1->next;
    pft2 = pft2->next;
    pft_new->next = TS_init_ft();
    pft_new->next->prev = pft_new;
    pft_new = pft_new->next;
    pft_new = TS_add_pointers(pft_new, pft1, pft2);
  }

  return pft_new;
}

s_ft *TS_secure_add_functions(s_ft *pftmp1, s_ft *pftmp2) {
  s_ft *pft_new;

  if (pftmp2 != NULL)
    pft_new = TS_add_functions(pftmp1, pftmp2);
  else
    pft_new = TS_copy_ts(pftmp1);

  pft_new = TS_browse_ft(pft_new, BEGINNING_TS);
  return pft_new;
}

/** \fn s_ft *TS_create_ts_from_file(char *name,FILE *flog)
 * \brief Creates a chain of ts from an input file with two columns( t ft).
 *
 *\return  the pointer to the beginning of the chain (first couple of the file being read)
 */
s_ft *TS_create_ts_from_file(char *name, FILE *flog) {

  s_ft *pft, *pftmp;
  FILE *fp;
  int eof;
  double t, ft;

  pft = NULL;
  pftmp = NULL;

  fp = fopen(name, "r");
  if (fp == NULL)
    LP_error(flog, "Impossible to open %s\n", name);

  eof = fscanf(fp, "%lf %lf\n", &t, &ft);
  while (eof != EOF) {
    pftmp = TS_create_function(t, ft);
    pft = TS_secured_chain_fwd_ts(pft, pftmp);
    eof = fscanf(fp, "%lf %lf\n", &t, &ft);
  }
  pft = TS_browse_ft(pft, BEGINNING_TS);
  fclose(fp); // FB 09/06/2016

  return pft;
}

/** \fn s_ft *TS_mean_fts(s_ft *pft1,s_ft *pft2)
 *\brief caculate the mean of two time series
 * \return at each time step the mean of the values of pft1->ft and pft2->ft
 * pft1 and pft2 must be defined at the same times
 */
s_ft *TS_mean_fts(s_ft *pft1, s_ft *pft2) {
  s_ft *pft_mean;

  pft_mean = TS_init_ft();

  pft1 = TS_browse_ft(pft1, BEGINNING_TS);
  pft2 = TS_browse_ft(pft2, BEGINNING_TS);

  // pft_mean->t = (pft1->t + pft2->t) / 2.;
  pft_mean->t = pft1->t;
  pft_mean->ft = (pft1->ft + pft2->ft) / 2.;

  while (pft1->next != NULL) {
    pft1 = pft1->next;
    pft2 = pft2->next;
    pft_mean->next = TS_init_ft();
    pft_mean->next->prev = pft_mean;
    pft_mean = pft_mean->next;
    // pft_mean->t = (pft1->t + pft2->t) / 2.;
    pft_mean->t = pft1->t;
    pft_mean->ft = (pft1->ft + pft2->ft) / 2.;
  }

  return pft_mean;
}

/** \fn s_ft *TS_substract_fts(s_ft *pft1,s_ft *pft2)
 *\brief caculates the diff of two time series
 * \return at each time step the diff of the values of pft1->ft and pft2->ft
 * pft1 and pft2 must be defined at the same times and to be the same length
 */
s_ft *TS_substract_fts(s_ft *pft1, s_ft *pft2) {
  s_ft *pft3;

  pft3 = TS_init_ft();
  pft3->t = pft1->t;
  pft3->ft = pft1->ft - pft2->ft;

  while (pft1->next != NULL) {
    pft1 = pft1->next;
    pft2 = pft2->next;
    pft3->next = TS_init_ft();
    pft3->next->prev = pft3;
    pft3 = pft3->next;
    pft3->t = pft1->t;
    pft3->ft = pft1->ft - pft2->ft;
  }

  pft1 = TS_browse_ft(pft1, BEGINNING_TS);
  pft2 = TS_browse_ft(pft2, BEGINNING_TS);
  pft3 = TS_browse_ft(pft3, BEGINNING_TS);

  return pft3;
}

/** \fn s_ft *TS_abs_substract_fts(s_ft *pft1,s_ft *pft2)
 *\brief caculate the absolute value of the diff of two time series
 * \return at each time step the the absolute value of the diff of the values of pft1->ft and pft2->ft
 * pft1 and pft2 must be defined at the same times and to be the same length
 */
s_ft *TS_abs_substract_fts(s_ft *pft1, s_ft *pft2) {
  s_ft *pft3;

  pft3 = TS_init_ft();
  pft3->t = pft1->t;
  pft3->ft = fabs(pft1->ft - pft2->ft);

  while (pft1->next != NULL) {
    pft1 = pft1->next;
    pft2 = pft2->next;
    pft3->next = TS_init_ft();
    pft3->next->prev = pft3;
    pft3 = pft3->next;
    pft3->t = pft1->t;
    pft3->ft = fabs(pft1->ft - pft2->ft);
  }

  pft1 = TS_browse_ft(pft1, BEGINNING_TS);
  pft2 = TS_browse_ft(pft2, BEGINNING_TS);
  pft3 = TS_browse_ft(pft3, BEGINNING_TS);

  return pft3;
}

/** \fn s_ft *TS_abs_ft(s_ft *pft)
 *\brief caculate the absolute value of pft
 */

s_ft *TS_abs_ft(s_ft *pft) {
  s_ft *pft1;
  pft1 = pft;
  while (pft != NULL) {
    pft1 = pft;
    pft->ft = fabs(pft->ft);
    pft = pft->next;
  }
  pft = pft1;
  pft = TS_browse_ft(pft, BEGINNING_TS);
  return pft;
}

/* Returns at each time step the division of the values of pft1->ft and pft2->ft
 * pft1 and pft2 must be defined at the same times
 */
s_ft *TS_divide_fts(s_ft *pftmp1, s_ft *pftmp2) {
  s_ft *pft_div = NULL;
  s_ft *pftmp;
  s_ft *pft1, *pft2;

  pft1 = pftmp1;
  pft2 = pftmp2;
  pft1 = TS_browse_ft(pft1, BEGINNING_TS);
  pft2 = TS_browse_ft(pft2, BEGINNING_TS);

  while (pft1 != NULL) {
    pftmp = TS_create_function(pft1->t, 0.);
    if (fabs(pft2->ft) > EPS_TS)
      pftmp->ft = pft1->ft / pft2->ft;
    else
      pftmp->ft = TS_INFINITE;
    pft_div = TS_secured_chain_fwd_ts(pft_div, pftmp);
    pft1 = pft1->next;
    pft2 = pft2->next;
  }

  pft_div = TS_browse_ft(pft_div, BEGINNING_TS);
  return pft_div;
}

/* Returns at each time step the multiplication of the values of pft1->ft and pft2->ft
 * pft1 and pft2 must be defined at the same times
 */
s_ft *TS_multiply_fts(s_ft *pftmp1, s_ft *pftmp2) {
  s_ft *pft_mul = NULL;
  s_ft *pftmp;
  s_ft *pft1, *pft2;

  pft1 = pftmp1;
  pft2 = pftmp2;
  pft1 = TS_browse_ft(pft1, BEGINNING_TS);
  pft2 = TS_browse_ft(pft2, BEGINNING_TS);

  while (pft1 != NULL) {
    pftmp = TS_create_function(pft1->t, 0.);
    pftmp->ft = pft1->ft * pft2->ft;
    pft_mul = TS_secured_chain_fwd_ts(pft_mul, pftmp);
    pft1 = pft1->next;
    pft2 = pft2->next;
  }

  pft_mul = TS_browse_ft(pft_mul, BEGINNING_TS);
  return pft_mul;
}

/** \fn s_ft *TS_division_ft_double(s_ft *pft,double div)
 *\brief caculate the division of a pft by a scalar
 * \return at each time step the division of the values of pft1->ft by div
 */

s_ft *TS_division_ft_double(s_ft *pft, double div, FILE *fpout) {
  s_ft *pft_div, *ptmp, *ptmp_div;

  // pft_div=(s_ft *) malloc(sizeof(s_ft));
  // bzero((char *)pft_div,sizeof(s_ft));
  pft_div = NULL;
  ptmp = TS_browse_ft(pft, BEGINNING_TS);

  while (ptmp != NULL) {

    ptmp_div = TS_create_function(ptmp->t, ptmp->ft / div);
    pft_div = TS_secured_chain_fwd_ts(pft_div, ptmp_div);

    ptmp = ptmp->next;
  }
  pft_div = TS_browse_ft(pft_div, BEGINNING_TS);
  return pft_div;
}

/** \fn s_ft *TS_division_ft_double(s_ft *pft,double div)
 *\brief caculate the multiplication of a pft by a scalar
 * \return  at each time step the multiplication of the values of pft1->ft by mult
 */

s_ft *TS_multiply_ft_double(s_ft *pft, double mult) {
  s_ft *pft_mult;

  pft_mult = TS_init_ft();

  pft = TS_browse_ft(pft, BEGINNING_TS);

  pft_mult->t = pft->t;
  pft_mult->ft = pft->ft * mult;

  while (pft->next != NULL) {
    pft = pft->next;
    pft_mult->next = TS_init_ft();
    pft_mult->next->prev = pft_mult;
    pft_mult = pft_mult->next;
    pft_mult->t = pft->t;
    pft_mult->ft = pft->ft * mult;
  }

  return pft_mult;
}

/** \fn s_ft *TS_free_ft(s_ft *pft)
 * \brief Deallocating a single _ft pointeur
 */
s_ft *TS_free_ft(s_ft *pft) {
  free(pft);
  return NULL;
}

/** \fn s_ft *TS_free_ft(s_ft *pft)
 * \brief Deallocating a full time series
 */
s_ft *TS_free_ts(s_ft *pft, FILE *fp) {
  s_ft *ptmp;
  s_ft *ptmp2;
  ptmp2 = TS_browse_ft(pft, BEGINNING_TS);
  pft = ptmp2;
  if (pft != NULL) {
    ptmp = pft;
    while (pft->next != NULL) {
      //  LP_printf(fp,"Deallocating %d\n",(int)pft->t);
      ptmp = pft->next;
      pft = TS_free_ft(pft);
      pft = ptmp;
    }
    pft = TS_free_ft(pft);

    if (pft != NULL)
      LP_warning(fp, "libts%4.2f: %s l%d _ft pointer not properly deallocated\n", VERSION_TS, __FILE__, __LINE__);
  }
  return pft;
}

/* Meme principe que function_value_t mais le sens de parcours est prédéfini, on ne repart pas du début à chaque fois ! */

/*WARNING le parcours est inverse dans cette fonction car elle s'appuie sur des chainage inverse temporellement du fait de l'utilisation de chain_function au lieu de TS_chain_fwd*/ // NF 27/3/2013

s_ft *TS_browse_tabulations(double t0, s_ft *pft_tmp, int begin_end, FILE *fp)

{
  s_ft *pft2, *pft;
  pft = pft_tmp;
  if (pft == NULL) {
    LP_error(fp, "In libts %4.2%f: TS_browse_tabulations() --> No values were given, and the calculation of the variable wasn't asked.\n", VERSION_TS);
  }

  if (begin_end == END_TS) { // Parcours dans le sens des H croissants
    if (pft->prev != NULL)
      pft2 = pft->prev;
    else
      pft2 = pft;

    while ((pft2->next != NULL) && (pft2->t < t0))
      pft2 = pft2->next;
  }

  else { // Parcours dans le sens des H décroissants
    if (pft->next != NULL)
      pft2 = pft->next;
    else
      pft2 = pft;

    while ((pft2->prev != NULL) && (pft2->t > t0))
      pft2 = pft2->prev;
  }

  return pft2;
}

double value_tabulations(double t0, s_ft *pft, int begin_end) {
  double ft;

  if (begin_end == END_TS) { // Parcours dans le sens des H croissants
    if (((pft->next == NULL) && (t0 > pft->t)) || (pft->prev == NULL))
      ft = pft->ft;
    else
      ft = TS_interpol(pft->prev->ft, pft->prev->t, pft->ft, pft->t, t0);
  }

  else { // Parcours dans le sens des H décroissants
    if (((pft->prev == NULL) && (t0 < pft->t)) || (pft->next == NULL))
      ft = pft->ft;
    else
      ft = TS_interpol(pft->next->ft, pft->next->t, pft->ft, pft->t, t0);
  }

  return ft;
}

double TS_value_tabulations(double t0, s_ft *pft, int begin_end, FILE *fp) {
  s_ft *pft2;
  double ft;

  pft2 = TS_browse_tabulations(t0, pft, begin_end, fp);
  ft = value_tabulations(t0, pft, begin_end);

  return ft;
}

/** \fn *TS_copy_ft(s_ft *pft)
 * \brief copy pft
 */
s_ft *TS_copy_ft(s_ft *pft) {
  s_ft *pcpy;
  pcpy = TS_create_function(pft->t, pft->ft);
  return pcpy;
}

/** \fn *TS_copy_ft(s_ft *pft)
 * \brief copy pft
 */
s_ft *TS_copy_ts(s_ft *pft) {
  s_ft *pcpy, *pcpy2;
  s_ft *ptmp;

  pcpy = NULL;
  ptmp = TS_browse_ft(pft, BEGINNING_TS);

  while (ptmp != NULL) {
    pcpy2 = TS_copy_ft(ptmp);
    pcpy = TS_secured_chain_fwd_ts(pcpy, pcpy2);
    ptmp = ptmp->next;
  }

  pcpy = TS_browse_ft(pcpy, BEGINNING_TS);
  return pcpy;
}

/**\fn s_ft *TS_function_t_pointed(double t0,s_ft *ptmp,FILE *fp)
 * \brief Puts the pointer ptmp the closest to t0; returns a pointer to s_ft.
 *
 * Notice that TS_function_value_t_pointed() returns only a double, but does not change the address of the input pointer
 * The current function instead returns a pointer with the correct address (the closest to t0) but we don't know if it's before or after t
 */

s_ft *TS_function_t_pointed(double t0, s_ft *pft2, FILE *fp)

{
  s_ft *pft;

  if (pft2 == NULL) {
    LP_error(fp, "libts %4.2f in TS_function_t_pointed() --> No time series sent to the routine\n", VERSION_TS);
  }

  // Gerer le fait que l'on tombe pile sur un point

  if (t0 > pft2->t) {
    while ((pft2->next != NULL) && (pft2->t < t0)) {
      pft2 = pft2->next;
    }
    if ((pft2->prev != NULL) && (pft2->t > t0)) {
      if (fabs(t0 - pft2->t) > fabs(pft2->t - pft2->prev->t) / 2) {
        pft = pft2->prev;
      } else
        pft = pft2;
    } else
      pft = pft2;

  }

  else {

    while ((pft2->prev != NULL) && (pft2->t > t0))
      pft2 = pft2->prev;
    if ((pft2->next != NULL) && (pft2->t < t0)) {
      if (fabs(t0 - pft2->t) > fabs(pft2->t - pft2->next->t) / 2)
        pft = pft2->next;
      else
        pft = pft2;
    } else
      pft = pft2;
  }

  return pft;
}

/**\fn double TS_affect_t(double t,s_ft *pft,int code)
 * \brief Checks the time t. If it is equal to CODE_TS, then the function returns the value of t at the BEGINNING or the END of the time series pft
 *
 */

double TS_affect_t(double t, s_ft *pft, int code) {
  double tvalue;

  if (fabs(t - CODE_TS) < EPS_TS) {
    if (code == BEGINNING_TS) {
      pft = TS_browse_ft(pft, BEGINNING_TS);
    } else
      pft = TS_browse_ft(pft, END_TS);
    tvalue = pft->t;
  } else
    tvalue = t;
  return tvalue;
}

/**\fn void TS_translate_t(s_ft *pft,double jref,FILE *fp)
 * \brief translates the time pft->t of jref for the whole time series
 *
 */

void TS_translate_t(s_ft *pft, double jref, FILE *fp) {
  s_ft *ptmp;

  ptmp = TS_browse_ft(pft, BEGINNING_TS);
  while (ptmp != NULL) {
    ptmp->t += jref;
    ptmp = ptmp->next;
  }
}

/**\fn void TS_auto_translate_t(s_ft *pft,FILE *fp)
 * \brief translates the time pft->t of the value of the first time (in integer) of the time series for the whole time series
 *
 */

void TS_auto_translate_t(s_ft *pft, FILE *fp) {
  s_ft *ptmp;
  double val;
  int ival;

  ptmp = TS_browse_ft(pft, BEGINNING_TS);
  // ival=floor(ptmp->t);
  // val=(double)ival;
  val = ptmp->t;
  val *= -1.;
  TS_translate_t(ptmp, val, fp);
}

/**\fn void TS_mult_t(s_ft *pft,double val,FILE *fp)
 * \brief multiples the time pft->t by val for the whole time series. Very usefull for time scaling in the frame of time series expressed by time steps with different time steps and scales. WARNING Usually it is safer to use this function after a translation to bring the origin of time to 0.
 *
 */

void TS_mult_t(s_ft *pft, double val, FILE *fp) {
  s_ft *ptmp;

  ptmp = TS_browse_ft(pft, BEGINNING_TS);
  while (ptmp != NULL) {
    ptmp->t *= val;
    ptmp = ptmp->next;
  }
}

/**\fn s_ft *TS_extract_chro_interpolate(s_ft *pft,double t0,double tf,FILE *fp)
 *\brief extract ft between time init (t0) and time end (tf). Interpolate the values at t0 and tf in two new pointers if necessary
 */
s_ft *TS_extract_chro_interpolate(s_ft *pft, double t0, double tf, FILE *fp) {
  s_ft *pftmp;
  s_ft *ppiece1, *ppiece2;
  double t;
  double t1, t2, f1, f2, ft;

  pftmp = pft;
  pftmp = TS_function_t_pointed(t0, pftmp, fp); // puts ptmp the closest to t0

  ppiece1 = NULL;
  ppiece2 = NULL;

  // test where we are with respect to t0
  if (fabs(pftmp->t - t0) > EPS_TS) {
    if (pftmp->t < t0) {
      t1 = pftmp->t;
      f1 = pftmp->ft;
      if (pftmp->next != NULL) {
        pftmp = pftmp->next;
        t2 = pftmp->t;
        f2 = pftmp->ft;
      } else {
        t2 = t1;
        f2 = f1;
      }
    } else {
      t2 = pftmp->t;
      f2 = pftmp->ft;
      if (pftmp->prev != NULL) {
        t1 = pftmp->prev->t;
        f1 = pftmp->prev->ft;
      } else {
        t1 = t2;
        f1 = f2;
      }
    }
    t = t0;
    ft = TS_interpol(f1, t1, f2, t2, t);
    ppiece1 = TS_create_function(t, ft);
  }
  /*  else	{ ppiece2=TS_copy_ft(ptmp);
    ptmp->ptmp->next;
    }*/ //Directly to the procedure

  t = pftmp->t;
  while ((t < tf) && (pftmp != NULL)) { // FB
    // printf("t<tf\n");
    if (fabs(pftmp->ft - CODE_TS) > EPS_TS) { // NF We do not extract a value which equals CODE_TS because it means no value!
      ppiece2 = TS_copy_ft(pftmp);
      ppiece1 = TS_secured_chain_fwd_ts(ppiece1, ppiece2);
    }
    ft = pftmp->t;
    t = pftmp->t;
    pftmp = pftmp->next;
    if (pftmp != NULL)
      t = pftmp->t;
  }

  if (pftmp != NULL) { // Create the last interpolated pointer
    if (fabs(pftmp->t - tf) > EPS_TS) {
      t1 = t2 = pftmp->t;
      f1 = f2 = pftmp->ft;
      if (pftmp->prev != NULL) {
        t1 = pftmp->prev->t;
        f1 = pftmp->prev->ft;
      }
      t = tf;
      ft = TS_interpol(f1, t1, f2, t2, t);
      ppiece2 = TS_create_function(t, ft);
    } else
      ppiece2 = TS_copy_ft(pftmp);
  } else {
    ppiece2 = TS_create_function(tf, ft);
  }

  ppiece1 = TS_secured_chain_fwd_ts(ppiece1, ppiece2);
  ppiece1 = TS_browse_ft(ppiece1, BEGINNING_TS);

  return ppiece1;
}

/**\fn s_ft *TS_extract_chro(s_ft *pft,int *nval,double t0,double tf,FILE *fp)
 *\brief extract ft between time init (t0) and time end (tf). The function feeds nval with the number of extracted values
 *
 * \return ppiece1 which is the extracted time_series made of pointer copies
 */
s_ft *TS_extract_chro(s_ft *pft, int *nval, double t0, double tf, FILE *fp) {
  s_ft *ptmp;
  s_ft *ppiece1, *ppiece2;
  double t;
  int n = 0;

  // printf("Dans extract chro\n");
  // getchar();
  ptmp = pft;
  // printf("ptmp->t=%f\n ptmp->ft=%f\n",ptmp->t,ptmp->ft);
  // getchar();
  ptmp = TS_function_t_pointed(t0, ptmp, fp); // puts ptmp the closest to t0
  // printf("ptmp->t=%f\n ptmp->ft=%f\n",ptmp->t,ptmp->ft);
  // getchar();
  // FB The following two lines added because we want to extract between t0 and tf, so we don't want values of t which are lower than t0. The TS_function_t_pointed can put the pointer either before or after t0 (the closest one of the two).
  if (ptmp->t < t0)
    ptmp = ptmp->next;
  ppiece1 = NULL;
  ppiece2 = NULL;
  t = ptmp->t;
  while ((t <= tf) && (ptmp != NULL)) { // FB
    // printf("t<tf\n");
    if (fabs(ptmp->ft - CODE_TS) > EPS_TS) { // NF We do not extract a value which equals CODE_TS because it means no value!
      n++;
      ppiece2 = TS_copy_ft(ptmp);
      ppiece1 = TS_secured_chain_fwd_ts(ppiece1, ppiece2);
    }
    ptmp = ptmp->next;
    if (ptmp != NULL)
      t = ptmp->t;
  }
  // printf("n=%d\n",n);
  *nval = n;
  ppiece1 = TS_browse_ft(ppiece1, BEGINNING_TS);
  return ppiece1;
}

/** \fn s_ft* TS_differentiate(s_ft *pft, double theta)
 * \brief differentiate a function theta is set between 0 and 1 it defines the
 * type of differentiation 0 upstream 1 downstrea 0.5 center
 */
s_ft *TS_differentiate_ts(s_ft *pft, double theta, FILE *flog) {
  s_ft *downstream = NULL, *upstream = NULL;
  double diff;
  s_ft *pfout = NULL, *ft_tmp;
  pft = TS_browse_ft(pft, BEGINNING_TS);
  // LP_printf(flog,"length of pft %d\n",TS_length_ts(pft));
  while (pft != NULL) {
    if (pft->next != NULL) {
      downstream = TS_create_function(pft->next->t, pft->next->ft);
    }
    if (pft->prev != NULL) {
      upstream = TS_create_function(pft->prev->t, pft->prev->ft);
    }
    if (upstream != NULL && downstream != NULL) {
      diff = theta * ((pft->ft - upstream->ft) / (pft->t - upstream->t)) + (1 - theta) * ((downstream->ft - pft->ft) / (downstream->t - pft->t));
      upstream = TS_free_ts(upstream, flog);
      downstream = TS_free_ts(downstream, flog);
    } else if (upstream == NULL) {
      diff = (downstream->ft - pft->ft) / (downstream->t - pft->t);
      downstream = TS_free_ts(downstream, flog);
    } else if (downstream == NULL) {
      diff = (pft->ft - upstream->ft) / (pft->t - upstream->t);
      upstream = TS_free_ts(upstream, flog);
    } else {
      LP_error(flog, "the time serie is only have one value\n");
    }
    ft_tmp = TS_create_function(pft->t, diff);
    pfout = TS_secured_chain_fwd_ts(pfout, ft_tmp);
    pft = pft->next;
  }
  return pfout;
}

/**\fn void TS_translate_ft(s_ft *pft,double jref,FILE *fp)
 * \brief translates the time pft->ft of jref for the whole time series
 *
 */

void TS_translate_ft(s_ft *pft, double jref, FILE *fp) {
  s_ft *ptmp;

  ptmp = TS_browse_ft(pft, BEGINNING_TS);
  while (ptmp != NULL) {
    ptmp->ft += jref;
    ptmp = ptmp->next;
  }
}

/** \fn double TS_integrate_ts(s_ft *pft,double ti,double tf,FILE *flog)
 * \brief integrate a function between two times (ti and tf). Each time step is integrated using TS_integrate.
 */
double TS_integrate_ts(s_ft *pft, double ti, double tf, FILE *flog) {

  s_ft *pftmp;
  double f = 0;
  double t1, t2, f1, f2;

  pftmp = TS_extract_chro_interpolate(pft, ti, tf, flog);
  // pftmp=TS_browse_ft(pft,BEGINNING_TS);
  while (pftmp->next != NULL) {
    t1 = pftmp->t;
    t2 = pftmp->next->t;
    f1 = pftmp->ft;
    f2 = pftmp->next->ft;
    f += TS_integrate(t1, f1, t2, f2);
    pftmp = pftmp->next;
  }
  pftmp = TS_free_ts(pftmp, flog);
  return f;
}

/** \fn double TS_integral_average_ts(s_ft *pft,double ti,double tf,FILE *flog)
 * \brief calculates the average based on the integration of ft() first. Slightly different from TS_weighted_average due to the trapezoidal integration and not a Riemann one (weighted average).
 */
double TS_integral_average_ts(s_ft *pft, double ti, double tf, FILE *flog) {

  double average;

  average = TS_integrate_ts(pft, ti, tf, flog);
  average /= (tf - ti);

  return average;
}

/** \fn s_ft *TS_complete_time_series(s_ft *pftmp1,s_ft *pftmp2)
 * \brief Example: given pftmp1=[t0,f(t0);t1,f(t1);...;t10,f(t10)] and pftmp2=[t3,g(t3);t5,g(t5);t7,g(t7)], this function gives the following time series: [t0,g(t3);t1,g(t3);t2,g(t3);t3,g(t3);t4,g(t3);t5,g(t5);t6,g(t5);t7,g(t7);t8,g(t7);t9,g(t7);t10,g(t7)].
 */
s_ft *TS_complete_time_series(s_ft *pftmp1, s_ft *pftmp2) {
  s_ft *pft_new = NULL, *ptmp = NULL;
  s_ft *pft1, *pft2;

  pft1 = TS_browse_ft(pftmp1, BEGINNING_TS);
  pft2 = TS_browse_ft(pftmp2, BEGINNING_TS);

  while (pft1->next != NULL) {
    if (pft2->next != NULL) {
      if (pft1->t < pft2->next->t) {
        ptmp = TS_create_function(pft1->t, pft2->ft);
      } else {
        pft2 = pft2->next;
        ptmp = TS_create_function(pft1->t, pft2->ft);
      }
    } else {
      ptmp = TS_create_function(pft1->t, pft2->ft);
    }
    pft_new = TS_secured_chain_fwd_ts(pft_new, ptmp);
    pft1 = pft1->next;
  }
  ptmp = TS_create_function(pft1->t, pft2->ft);
  pft_new = TS_secured_chain_fwd_ts(pft_new, ptmp);

  pft_new = TS_browse_ft(pft_new, BEGINNING_TS);

  return pft_new;
}

/** \fn s_date_ts **TS_get_ts_border(s_ft *pft,int yr0,FILE *flog)
 * \brief gets the date of beginning and end of a time series. Returns an array of two pointers s_date_ts*, in pdate[BEGINNING_TS] and  pdate[END_TS], respectively.
 */
s_date_ts **TS_get_ts_border(s_ft *pft, int yr0, FILE *flog) {

  s_date_ts **pdate;
  int i;
  s_ft *ptmp;

  if (yr0 == CODE_TS)
    yr0 = INITIAL_YEAR_JULIAN_DAY_TS;

  pdate = ((s_date_ts **)malloc(NEXTREMA_TS * sizeof(s_date_ts *)));

  for (i = 0; i < NEXTREMA_TS; i++) {
    pdate[i] = new_date_ts();
    bzero((char *)pdate[i], sizeof(s_date_ts));
    ptmp = TS_browse_ft(pft, i);
    pdate[i] = TS_convert_julian2date_decimal((double)ptmp->t, yr0, flog);
  }

  return pdate;
}
