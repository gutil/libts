/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libts
 * FILE NAME: wodic_cpl.c
 *
 * CONTRIBUTORS: Nicolas FLIPO, Lauriane VILMIN, Shuaitao WANG,
 *               Baptiste LABARTHE, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: Management of time series including linear
 * interpolation for holes as well as convertions back and forth between
 * julian days and calendar dates.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libts Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <math.h>
#include <string.h>
#include "libprint.h"
#include "time_series.h"

/**\fn void TS_print_ts_for_hydsep(s_ft *pft,FILE *fp)
 *\brief print day  yyy-mm-jj ft in column in FILE *fp, where day=floor(t)
 *flog is the log file
 */
void TS_print_ts_for_hydsep(s_ft *pft, FILE *fp, FILE *flog) {
  s_ft *ptmp;
  s_date_ts *pd;
  char sep[STRING_LENGTH_LP];
  sprintf(sep, "-");

  ptmp = TS_browse_ft(pft, BEGINNING_TS);

  while (ptmp != NULL) {

    // LP_printf(fp,"%d ",floor(ptmp->t));//FB 02/06/16
    LP_printf(fp, "%d ", (int)ptmp->t); // FB 02/06/16
    pd = TS_convert_julian2date_decimal(ptmp->t, CODE_TS, flog);
    TS_print_date_sep(pd, US_TS, sep, fp, flog);
    LP_printf(fp, " %f\n", ptmp->ft);
    ptmp = ptmp->next;
  }
}
