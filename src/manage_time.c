/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libts
 * FILE NAME: manage_time.c
 *
 * CONTRIBUTORS: Nicolas FLIPO, Lauriane VILMIN, Shuaitao WANG,
 *               Baptiste LABARTHE, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: Management of time series including linear
 * interpolation for holes as well as convertions back and forth between
 * julian days and calendar dates.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libts Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
/**\fn int TS_is_bissex(int nj)
 * \brief bissex yes or no
 *
 */
int TS_is_bissex(int nj) {
  int bissex = NO_TS;
  if (nj > 365)
    bissex = YES_TS;
  return bissex;
}

/**\fn float TS_def_calc_bissex(int type)
 *\brief algorithm to detect bissextile yers and to calculate the number of days in year "y"
 *
 *Warning : milleniums and centruries are not implemented
 */

float TS_def_calc_bissex(int type) {
  float denum;
  switch (type) {
  case NORMAL_TS:
    denum = 4.;
    break;
  case CENTURY_TS:
    denum = 100.;
    break;
  case FOURCENTURY_TS:
    denum = 400.;
    break;
  }
  return denum;
}

/**\fn int TS_year_type(int an,int type)
 *\brief defineing if year "an" is bissex
 *
 *
 */
int TS_year_type(int an, int type) {
  int bissex = NO_TS;
  float an_test;
  int test;
  float denum;

  denum = TS_def_calc_bissex(type);
  an_test = an / denum;
  test = (int)an_test;
  // printf("test %s de %d, partie entière %d/%f = %d\n",name_bissex(type),an,an,denum,test);

  test *= (int)denum;

  if (test == an) {
    bissex = YES_TS;
    // printf("test %s annee hydrologique bissextile detectee : %d\n",name_bissex(type),an);
  }
  /* else
     printf("test %s annee hydrologique %d non bissextile\n",name_bissex(type),an);*/

  return bissex;
}

/**\fn int TS_nb_jour_an(int an)
 *\return nb days in year an
 *
 *
 */
int TS_nb_jour_an(int an) {
  int nj = 365;
  int bissex = NO_TS;
  int tmp_c, tmp_m;

  bissex = TS_year_type(an, NORMAL_TS);
  if (bissex == YES_TS) {
    tmp_c = TS_year_type(an, CENTURY_TS);
    if (tmp_c == YES_TS) {
      bissex = NO_TS;
      tmp_m = TS_year_type(an, FOURCENTURY_TS);
      if (tmp_m == YES_TS)
        bissex = YES_TS;
    }
  }
  if (bissex == YES_TS)
    nj = 366;

  return nj;
}

/**\fn int TS_nday_month(int month)
 *\brief Calculates the number of day of a month for a regular year (365 days)
 *
 *
 */
int TS_nday_month(int month) {
  int nj = 0;

  switch (month) {
  case JANUARY:
    nj = 31;
    break;
  case FEBRUARY:
    nj = 28;
    break;
  case MARCH:
    nj = 31;
    break;
  case APRIL:
    nj = 30;
    break;
  case MAY:
    nj = 31;
    break;
  case JUNE:
    nj = 30;
    break;
  case JULY:
    nj = 31;
    break;
  case AUGUST:
    nj = 31;
    break;
  case SEPTEMBER:
    nj = 30;
    break;
  case OCTOBER:
    nj = 31;
    break;
  case NOVEMBER:
    nj = 30;
    break;
  case DECEMBER:
    nj = 31;
    break;
  }

  return nj;
}

/**\fn int TS_nday_cum_month(int month)
 *\brief Calculates the number of days until the beginning of a year for a given month of a regular year (365 days)
 *
 */
int TS_nday_cum_month(int month) {
  int nj = 0;
  int i;
  for (i = 0; i < month; i++)
    nj += TS_nday_month(i);
  return nj;
}

/**\fn int TS_calculate_nd_month(int year,int month)
 *\brief calculate the number of days until the 1rst day 00:00 of "month" of "year"
 *yr0 is the julian day of reference, by default january 1th 1900
 */
int TS_calculate_nd_month(int year, int month, int yr0) {
  int njyr, nj;
  /*il faut rajouter le nb de jour jusqu'au 1er aout de year*/
  njyr = TS_nb_jour_an(year);
  nj = TS_nday_cum_month(month);
  if ((njyr == BISSEXTILE_TS) && (month > FEBRUARY))
    nj++;
  return nj;
}

/**\fn int TS_calculate_jour_julien(int year,int yr0,int month)
 *\brief calculate julian day on 1th 00:00 of "month" of "year"
 * To account for a full year until 31 of december month must be equal to NMONTHS_TS and not DECEMBER, otherwise the end is december 1rst.
 * initial julian day is 1900 january 1th
 */
int TS_calculate_jour_julien(int year, int yr0, int month) {

  int jj = 0;
  int yr;
  int nj = 0, njyr;

  if (yr0 == CODE_TS)
    yr0 = INITIAL_YEAR_JULIAN_DAY_TS;

  yr = yr0;
  // NF 19/10/2014 I add this to be able to consider a full year until december 31. In this case just send NMONTHs_TS instead of the month itself
  if (month >= NMONTHS_TS) {
    month = JANUARY;
    year++;
  }

  while (yr < year) {
    jj += TS_nb_jour_an(yr++);
  }
  /*il faut rajouter le nb de jour jusqu'au 1er day of "month"*/
  nj = TS_calculate_nd_month(year, month, yr0);
  jj += nj;
  return jj;
}

/**\fn int TS_calculate_jour_julien_j_nbday(int year,int yr0,int nbday)
 *\brief calculate julian day on year + nbday from the 1st august of year
 *
 * initial julian day is 1900 january 1th
 */
int TS_calculate_jour_julien_j_nbday(int year, int yr0, int nb_day) // TVdebug old function//NF 13/2/2020 modif TV is dirty and destroy other function. I don't know where it is done but needs a specific function
// double TS_calculate_jour_julien_j_nbday(int year,int yr0,int nb_day,double dt) //TVdebug new function
{

  // int jj=0;
  double jj = 0.0;
  int yr;
  int nj = 0, njyr;
  int month;
  month = AUGUST;
  if (yr0 == CODE_TS)
    yr0 = INITIAL_YEAR_JULIAN_DAY_TS;

  yr = yr0;

  while (yr < year) {
    jj += TS_nb_jour_an(yr++);
  }
  /*il faut rajouter le nb de jour jusqu'au 1er day of "month"*/
  nj = TS_calculate_nd_month(year, month, yr0);
  jj += nj;
  jj += (nb_day);
  // jj+=(nb_day*dt/86400.0)+(dt/86400.0);//NF 13/2/2020 remove TV bugs
  return jj;
}
/**\fn void  TS_check_nb_day(int year,int month,int day,FILE *fp)
 *\brief check nb days in month
 *
 */
void TS_check_nb_day(int year, int month, int day, FILE *fp) {
  int nj, njmax;

  njmax = TS_nday_month(month);
  nj = TS_nb_jour_an(year);

  if (day > njmax) {
    if ((nj == BISSEXTILE_TS) && (month == FEBRUARY)) {
      njmax++;
      if (day > njmax) {
        LP_error(fp, "In lts%4.2f %s line %d : Wrong date sent to  TS_check_nb_day(),%s does not have more than %d days. %d%d%d was sent?", VERSION_TS, __FILE__, __LINE__, TS_name_month(month, fp), njmax, year, month, day);
      }
    } else
      LP_error(fp, "In lts%4.2f %s line %d : Wrong date sent to  TS_check_nb_day(),%s does not have more than %d days. %d%d%d was sent?", VERSION_TS, __FILE__, __LINE__, TS_name_month(month, fp), njmax, year, month, day);
  }
}
/**\fn int TS_calculate_jour_julien_j(int year,int yr0,int month,int day,FILE *fp)
 *\brief calculate julian day dd/mm/yyyy from 01/01/yr0 00:00
 *
 */
int TS_calculate_jour_julien_j(int year, int yr0, int month, int day, FILE *fp) {

  int jj = 0;

  jj = TS_calculate_jour_julien(year, yr0, month);
  TS_check_nb_day(year, month, day, fp);
  day--;
  jj += day;

  return jj;
}

/**\fn void TS_create_monthly_ts(s_ft *ptmp,s_ft **pmft,FILE *fp,int yr0,int yr_ini,int yr_end,int month_ini,int month_end)
 *\brief create monthly ft in year "year", between "month_ini" "month_end"
 *
 */
void TS_create_monthly_ts(s_ft *ptmp, s_ft **pmft, FILE *fp, int yr0, int yr_ini, int yr_end, int month_ini, int month_end) {

  s_ft *pft, *pftmo, *pftmotmp, *p2;
  int j, m, yr;
  int nj_ini;
  int monini, monend;
  int jini, jend;
  double dini, dend;
  int nval = 0;

  if (yr0 == CODE_TS)
    yr0 = INITIAL_YEAR_JULIAN_DAY_TS;

  pft = TS_browse_ft(ptmp, BEGINNING_TS);

  nj_ini = TS_calculate_jour_julien(yr_ini, CODE_TS, month_ini);

  for (yr = yr_ini; yr <= yr_end; yr++) {
    monini = JANUARY;
    monend = DECEMBER;
    if (yr == yr_ini)
      monini = month_ini;
    if (yr == yr_end)
      monend = month_end;
#ifdef DEBUG
    LP_printf(fp, "Starting %d  %s -> %s\n", yr, TS_name_month(monini, fp), TS_name_month(monend, fp));
#endif
    for (m = monini; m <= monend; m++) {
      jini = TS_calculate_jour_julien(yr, yr0, m);
      jend = TS_calculate_jour_julien(yr, yr0, m + 1);
      dini = (double)jini;
      dend = (double)jend;
      dend -= EPS2_TS;
      pftmo = pmft[m];

      pftmotmp = TS_extract_chro(pft, &nval, dini, dend, fp);
      if (pftmotmp != NULL) {
        pftmotmp = TS_browse_ft(pftmotmp, BEGINNING_TS);
        pftmo = TS_secured_chain_fwd_ts(pftmo, pftmotmp);
        pftmo = TS_browse_ft(pftmo, END_TS);
      }
      /* for (j=jini;j<jend;j++) {
        //pftmotmp=TS_create_function(j-nj_ini,pft->ft);

        pftmotmp=TS_create_function(j,pft->ft);//NF 4/10/2016 je prefere conserver la date
        pftmo=TS_secured_chain_fwd_ts(pftmo,pftmotmp);
        pft=pft->next;
        }
        }*/

      // pmft[m]=TS_browse_ft(pftmo,BEGINNING_TS);
      pmft[m] = pftmo;
    }
  }
  for (m = 0; m < NMONTHS_TS; m++)
    pmft[m] = TS_browse_ft(pmft[m], BEGINNING_TS);
}

/**\fn void void TS_create_annual_ts(s_ft *ptmp,s_ft **pmft,FILE *fp,int yr0,int yr_ini,int yr_end,int month_ini,int month_end)
 *\brief create annual ft in year "year", between "month_ini" "month_end"
 *
 */
void TS_create_annual_ts(s_ft *ptmp, s_ft **pmft, FILE *fp, int yr0, int yr_ini, int yr_end, int month_ini, int month_end) {

  s_ft *pft, *pftmo, *pftmotmp;
  int i, j, m, yr;
  int nj_ini;
  int monini = JANUARY, monend = DECEMBER;
  int jini, jend;
  int nyr;
  int complete[NEXTREMA_TS];
  int borne[NEXTREMA_TS];
  double tmp;
  s_carac_ts *pcts;

  pcts = TS_carac_ts(ptmp, yr0, fp); // NF 28/10/2014

  nyr = yr_end - yr_ini + 1;
  //  pmft=((s_ft**)calloc(nyr,sizeof(s_ft*)));
  if (yr0 == CODE_TS)
    yr0 = INITIAL_YEAR_JULIAN_DAY_TS;

  pft = TS_browse_ft(ptmp, BEGINNING_TS);

  nj_ini = TS_calculate_jour_julien(yr_ini, yr0, month_ini);

  for (yr = yr_ini; yr <= yr_end; yr++) {
    /* if(yr==2012)
       LP_printf(stderr,"In libts%4.2f %s l.%d:effacer ces deux lignes\n",VERSION_TS,__FILE__,__LINE__);*/
    jini = TS_calculate_jour_julien(yr, yr0, monini);
    jend = TS_calculate_jour_julien(yr + 1, yr0, monini);
    i = yr - yr_ini;
    pftmo = pmft[i];

    borne[BEGINNING_TS] = TS_calculate_jour_julien(yr, yr0, monini);
    borne[END_TS] = TS_calculate_jour_julien(yr + 1, yr0, monini);

    if (yr == yr_ini) {
      if (monini != month_ini) {
        borne[BEGINNING_TS] = TS_calculate_jour_julien(yr, yr0, month_ini);
      }
      if (pcts->pd[BEGINNING_TS]->dd != 1)
        borne[BEGINNING_TS] += pcts->pd[BEGINNING_TS]->dd - 1; // NF 28/10/2014 needs to add that in the case of a time series which doesn't start the first of a month
    }
    if (yr == yr_end) {
      if (monend != month_end)
        borne[END_TS] = TS_calculate_jour_julien(yr, yr0, month_end);
    }
#ifdef DEBUG
    LP_printf(fp, "Starting %d  %s -> %s\n", yr, TS_name_month(monini, fp), TS_name_month(monend, fp));
#endif

    j = jini;

    for (j = jini; j < borne[BEGINNING_TS]; j++) {
      pftmotmp = TS_create_function(j, CODE_TS);
      pftmo = TS_secured_chain_fwd_ts(pftmo, pftmotmp);
    }
    // NF 28/10/2014 We need a while condition for not regurlarly constinuous time series
    j = (int)pft->t;
    while (j < borne[END_TS]) {
      pftmotmp = TS_create_function(j, pft->ft);
      pftmo = TS_secured_chain_fwd_ts(pftmo, pftmotmp);
      pft = pft->next;
      if (pft != NULL)
        j = (int)pft->t;
      else // NF 27/10/2014 Add this line to be able to treat a year which does not go until the end of a month. Otherwise seg fault
        j = borne[END_TS];
    }
    /*for(j=borne[BEGINNING_TS];j<borne[END_TS];j++){
        pftmotmp=TS_create_function(j,pft->ft);
        pftmo=TS_secured_chain_fwd_ts(pftmo,pftmotmp);
        pft=pft->next;
        if (pft==NULL)//NF 27/10/2014 Add this line to be able to treat a year which does not go until the end of a month. Otherwise seg fault
          j=borne[END_TS];
          }*/
    for (j = borne[END_TS]; j < jend; j++) {
      pftmotmp = TS_create_function(j, CODE_TS);
      pftmo = TS_secured_chain_fwd_ts(pftmo, pftmotmp);
    }
    pmft[i] = pftmo;
  }
  for (i = 0; i < nyr; i++) {
    pmft[i] = TS_browse_ft(pmft[i], BEGINNING_TS);
  }
}

/** double TS_calc_julian_decimal(int hh,int mm,int ss,FILE *flog)
 * \brief converts hh:mm:ss in julian decimals with HOUR_START beeing 0, and Hour_scale beeing 24h. This is the core function of libts for time decimal calculation, but hours hh must be processed by int TS_process_time(int hhini,int *hhf,int day_time,int hr_scale,int hr_offset,FILE *flog) beforehand. DO NOT USE TS_calc_julian_decimal(int,int,int,FILE *) STRAIGHT.
 *
 *\return  the hour converted in decimals
 */

double TS_calc_julian_decimal(int hh, int mm, int ss, FILE *flog) {

  double ddec = 0.;
  double tmp = 0.;

  ddec = (double)hh;
  ddec *= NMIN_HOUR_TS;
  ddec += (double)mm;
  ddec *= NSEC_MIN_TS;
  ddec += (double)ss;
  ddec *= CONV_SEC_DDEC_TS;

  return ddec;
}

/** \fn s_ft *TS_create_from_file_date(char *name,int icode,int yr0,FILE *flog)
 * \brief Creates a chain of ts from an input file with two columns. if icode=FR_TS or CODE_TS (jj/mm/yyyy ft). if icode=US_TS (yyyy/mm/dd ft).yr0 defines the origin of time, equals INITIAL_YEAR_JULIAN_DAY_TS if set up to CODE_TS
 *
 *\return  the pointer to the beginning of the chain (first couple of the file being read)
 */
s_ft *TS_create_from_file_date(char *name, int icode, int yr0, FILE *flog) // NF 28/11/2020 adding yr0 argument before FILE
{

  s_ft *pft, *pftmp;
  FILE *fp;
  int eof;
  double t, ft;
  int dd, mm, yyyy;

  if (yr0 == CODE_TS)
    yr0 = INITIAL_YEAR_JULIAN_DAY_TS; // NF 28/11/2020

  pft = NULL;
  pftmp = NULL;
  if (icode == CODE_TS)
    icode = FR_TS;

  fp = fopen(name, "r");
  if (fp == NULL)
    LP_error(flog, "Impossible to open %s\n", name);
  switch (icode) {
  case FR_TS:
    eof = fscanf(fp, "%2d/%2d/%4d %lf\n", &dd, &mm, &yyyy, &ft);
    break;
  case US_TS:
    eof = fscanf(fp, "%4d/%2d/%2d %lf\n", &dd, &mm, &yyyy, &ft);
    break;
  default:
    LP_error(flog, "In libts%4.2f TS_create_from_file_date() %s l.%d: Encoded date unknown, impossible to read%s\n", VERSION_TS, __FILE__, __LINE__, name);
    break;
  }
  while (eof != EOF) {
    t = TS_calculate_jour_julien_j(yyyy, yr0, --mm, dd, flog); // NF 28/11/2020
    pftmp = TS_create_function(t, ft);
    pft = TS_secured_chain_fwd_ts(pft, pftmp);
    switch (icode) {
    case FR_TS:
      eof = fscanf(fp, "%2d/%2d/%4d %lf\n", &dd, &mm, &yyyy, &ft);
      break;
    case US_TS:
      eof = fscanf(fp, "%4d/%2d/%2d %lf\n", &dd, &mm, &yyyy, &ft);
      break;
    }
  }
  pft = TS_browse_ft(pft, BEGINNING_TS);

  return pft;
}

/** int TS_modify_hr(int hh,int hr_scale,int hr_offset,int day_time,FILE *flog)
 * \brief modifies the hour to put it in the reference system of libts with a time scale of 24h, and an offset of 0h, ie at midnight it is Oh
 *
 *\return  hhn which is an hour processable by libts functions
 */

int TS_modify_hr(int hh, int hr_scale, int hr_offset, int day_time, FILE *flog) {
  int hhn;
  hhn = hh;
  if (hr_scale != HOUR_SCALE_TS) {
    switch (day_time) {
    case AM_TS:
      break;
    case PM_TS:
      hhn += hr_scale;
      if (hhn >= HOUR_SCALE_TS)
        hhn -= HOUR_SCALE_TS;
      break;
    default:
      LP_warning(flog, "In libts%4.2f file %s l.%d: Specified day_time unknown for time conversion. Ignored, day_time set to AM_TS. This may creates computation problems\n", VERSION_TS, __FILE__, __LINE__);
      hhn = TS_modify_hr(hh, hr_scale, hr_offset, AM_TS, flog);
      break;
    }
  }
  hhn -= hr_offset;
  return hhn;
}

/** int TS_switch_time(int iday_code)
 * \brief switch the clock from AM to PM for tricky instruments as HOBBO sensors.
 *
 *\return  iday_code equals AM_TS or PM_TS
 */
int TS_switch_time(int iday_code) {
  if (iday_code == AM_TS)
    iday_code = PM_TS;
  else
    iday_code = AM_TS;
  return iday_code;
}

/** int TS_process_time(int hhini,int *hhf,int day_time,int hr_scale,FILE *flog)
 * \brief processes tricky sensors as HOBBO to first determine the period of the day when the measurement started. And second to manage to switch the clock from AM_TS to PM_TS properly in the case of a 12 hr-time scale. WARNING *hhf is modified by the function, so that hini is always on a 24hr-scale
 *
 *\return  iday_code
 */
int TS_process_time(int hhini, int *hhf, int day_time, int hr_scale, int hr_offset, FILE *flog) {
  int iday_code;
  int hhtmp;

  iday_code = day_time;

  if (hr_scale == CODE_TS)
    hr_scale = HOUR_SCALE_TS;
  if (hr_offset == CODE_TS)
    hr_offset = HOUR_START_TS;

  if (hhini != CODE_TS) {
    if (hr_scale != HOUR_SCALE_TS) { // ie	case HOBBO_TS it is a nightmare, and a big headhache
      // on a le iday_code d'avant. a priori on reste sur la même gamme sauf en cas de passage de AM_TS à PM_TS
      if (*hhf != 12) {
        // Dans le cas le capteur hobbo indique 12h pour midi ce qui est OK mais on ne veut pas lui ajouter 12 heure, soit il indique 12 pour minuit et donc on veut lui ajouter 12hr - 24hr. Il ne faut donc pas changer le iday_code dans le cas *hhf==12, mais au passage 12->1, ie si hhf!=hini et hini==12
        // Sinon il faut assurer le passage de AM_Ts a PM_TS et vice versa
        // C'est le cas ou hhf ==1
        hhtmp = TS_modify_hr(*hhf, hr_scale, hr_offset, iday_code, flog); // Je dois faire cette transformation car hini est deja au format 24h
        if (hhtmp != hhini) {
          if ((hhini == 12) || (hhini == 0))
            iday_code = TS_switch_time(iday_code);
        }
      }
    }
  } else {
    if (hr_scale != HOUR_SCALE_TS) {
      // ie	case HOBBO_TS or based on time when a sensor can be launched on the field, ie it is the first measurment
      if ((1 <= *hhf) && (*hhf <= 8))
        iday_code = PM_TS;
      else {
        if ((9 <= *hhf) && (*hhf <= 12))
          iday_code = AM_TS;
        else
          LP_error(flog, "In libts%4.2f file %s l.%d: Impossible to determine the beginning hour in the sequence. hour %d incompatible with with time scale %dh . hr_scale should be HOUR_SCALE_TS, ie 24h. Check the compatibility of the data and the algorithme, maybe some issue with iinstrument. Contact libts developpers\n", VERSION_TS, __FILE__, __LINE__, *hhf, hr_scale);
      }
    }
  }
  *hhf = TS_modify_hr(*hhf, hr_scale, hr_offset, iday_code, flog);
  return iday_code;
}
/** \fn s_ft *TS_create_from_file_date_hh(char *name,int icode,int iinstru,int yr0,FILE *flog)
 * \brief Creates a chain of ts from an input file with three columns. if icode=FR_TS or CODE_TS (jj/mm/yyyy hh:mm:ss ft). if icode=US_TS (yyyy/mm/dd  hh:mm:ss ft). iinstru specifies the instrument used to set up the time scale. yr0 defines the origin of time, equals INITIAL_YEAR_JULIAN_DAY_TS if set up to CODE_TS
 *
 *\return  the pointer to the beginning of the chain (first couple of the file being read)
 */
s_ft *TS_create_from_file_date_hh(char *name, int icode, int iinstru, int yr0, FILE *flog) // NF 28/11/2020 adding yr0 argument before FILE
{

  s_ft *pft, *pftmp;
  FILE *fp;
  int eof;
  double t, ft;
  int dd, mm, yyyy;
  int hh, min, ss;
  int hr_scale = HOUR_SCALE_TS;
  int hr_offset = HOUR_START_TS;
  int day_time = DAY_TIME_TS;
  int day_code;
  int hhini = CODE_TS;

  if (yr0 == CODE_TS)
    yr0 = INITIAL_YEAR_JULIAN_DAY_TS; // NF 28/11/2020

  if (iinstru == HOBBO_TS) {
    hr_scale = HOBBO_SCALE_TS;
    hr_offset = HOBBO_OFFSET_TS;
    day_time = HOBBO_DAY_TIME_TS;
    icode = MIX_TS;
  }
  day_code = day_time;
  pft = NULL;
  pftmp = NULL;
  if (icode == CODE_TS)
    icode = FR_TS;

  fp = fopen(name, "r");
  if (fp == NULL)
    LP_error(flog, "Impossible to open %s\n", name);
  switch (icode) {
  case FR_TS:
    eof = fscanf(fp, "%d/%d/%4d %d:%d:%d  %lf\n", &dd, &mm, &yyyy, &hh, &min, &ss, &ft);
    break;
  case US_TS:
    eof = fscanf(fp, "%4d/%d/%d %d:%d:%d  %lf\n", &yyyy, &mm, &dd, &hh, &min, &ss, &ft);
    break;
  case MIX_TS:
    eof = fscanf(fp, "%d/%d/%4d %d:%d:%d  %lf\n", &mm, &dd, &yyyy, &hh, &min, &ss, &ft);
    break;
  default:
    LP_error(flog, "In libts%4.2f TS_create_from_file_date() %s l.%d: Encoded date unknown, impossible to read%s\n", VERSION_TS, __FILE__, __LINE__, name);
    break;
  }
  while (eof != EOF) {
    t = TS_calculate_jour_julien_j(yyyy, yr0, --mm, dd, flog); // NF 28/11/2020
    day_code = TS_process_time(hhini, &hh, day_code, hr_scale, hr_offset, flog);
    t += TS_calc_julian_decimal(hh, min, ss, flog);
    hhini = hh; // Here the time is always on a 24h-time scale
    pftmp = TS_create_function(t, ft);
    pft = TS_secured_chain_fwd_ts(pft, pftmp);
    switch (icode) {
    case FR_TS:
      eof = fscanf(fp, "%d/%d/%4d %d:%d:%d  %lf\n", &dd, &mm, &yyyy, &hh, &min, &ss, &ft);
      break;
    case US_TS:
      eof = fscanf(fp, "%4d/%d/%d %d:%d:%d  %lf\n", &yyyy, &mm, &dd, &hh, &min, &ss, &ft);
      break;
    case MIX_TS:
      eof = fscanf(fp, "%d/%d/%4d %d:%d:%d  %lf\n", &mm, &dd, &yyyy, &hh, &min, &ss, &ft);
      break;
    }
  }
  pft = TS_browse_ft(pft, BEGINNING_TS);

  return pft;
}

/** \fn s_ft *TS_create_from_file_date_hh(char *name,int icode,int iinstru,int yr0,FILE *flog)
 * \brief Creates a chain of ts from an input file with three columns. if icode=FR_TS or CODE_TS (jj/mm/yyyy hh:mm ft). if icode=US_TS (yyyy/mm/dd  hh:mm ft). iinstru specifies the instrument used to set up the time scale. yr0 defines the origin of time, equals INITIAL_YEAR_JULIAN_DAY_TS if set up to CODE_TS
 *
 *\return  the pointer to the beginning of the chain (first couple of the file being read)
 */
s_ft *TS_create_from_file_date_hh_mm(char *name, int icode, int iinstru, int yr0, FILE *flog) // NF 28/11/2020 adding yr0 argument before FILE
{

  s_ft *pft, *pftmp;
  FILE *fp;
  int eof = CODE_TS;
  double t, ft;
  int dd, mm, yyyy;
  int hh, min, ss = 0;
  int hr_scale = HOUR_SCALE_TS;
  int hr_offset = HOUR_START_TS;
  int day_time = DAY_TIME_TS;
  int day_code;
  int hhini = CODE_TS;

  if (yr0 == CODE_TS)
    yr0 = INITIAL_YEAR_JULIAN_DAY_TS; // NF 28/11/2020

  if (iinstru == HOBBO_TS) {
    hr_scale = HOBBO_SCALE_TS;
    hr_offset = HOBBO_OFFSET_TS;
    day_time = HOBBO_DAY_TIME_TS;
    icode = MIX_TS;
  }
  day_code = day_time;
  pft = NULL;
  pftmp = NULL;
  if (icode == CODE_TS)
    icode = FR_TS;

  fp = fopen(name, "r");
  if (fp == NULL)
    LP_error(flog, "Impossible to open %s\n", name);
  switch (icode) {
  case FR_TS:
    eof = fscanf(fp, "%d/%d/%4d %d:%d %lf\n", &dd, &mm, &yyyy, &hh, &min, &ft);
    break;
  case US_TS:
    eof = fscanf(fp, "%4d/%d/%d %d:%d %lf\n", &yyyy, &mm, &dd, &hh, &min, &ft);
    break;
  case MIX_TS:
    eof = fscanf(fp, "%d/%d/%4d %d:%d %lf\n", &mm, &dd, &yyyy, &hh, &min, &ft);
    break;
  default:
    LP_error(flog, "In libts%4.2f TS_create_from_file_date() %s l.%d: Encoded date unknown, impossible to read%s\n", VERSION_TS, __FILE__, __LINE__, name);
    break;
  }
  while (eof != EOF) {
    t = TS_calculate_jour_julien_j(yyyy, yr0, --mm, dd, flog); // NF 28/11/2020
    day_code = TS_process_time(hhini, &hh, day_code, hr_scale, hr_offset, flog);
    t += TS_calc_julian_decimal(hh, min, ss, flog);
    hhini = hh; // Here the time is always on a 24h-time scale
    pftmp = TS_create_function(t, ft);
    pft = TS_secured_chain_fwd_ts(pft, pftmp);
    switch (icode) {
    case FR_TS:
      eof = fscanf(fp, "%d/%d/%4d %d:%d %lf\n", &dd, &mm, &yyyy, &hh, &min, &ft);
      break;
    case US_TS:
      eof = fscanf(fp, "%4d/%d/%d %d:%d %lf\n", &yyyy, &mm, &dd, &hh, &min, &ft);
      break;
    case MIX_TS:
      eof = fscanf(fp, "%d/%d/%4d %d:%d %lf\n", &mm, &dd, &yyyy, &hh, &min, &ft);
      break;
    }
  }
  pft = TS_browse_ft(pft, BEGINNING_TS);

  return pft;
}

/** \fn s_date_ts *TS_convert_julian2date(int ijulian,int yr0,FILE *flog)
 * \brief converts a julian day (integer) to a date(jj/mm/yyyy) that is put in a date pointer.
 *
 *\return  the pointer to the date
 */
s_date_ts *TS_convert_julian2date(int ijulian, int yr0, FILE *flog) {
  int yr;
  int jj = 0, mm;
  int dd = 0;

  s_date_ts *pd;
  if (yr0 == CODE_TS)
    yr0 = INITIAL_YEAR_JULIAN_DAY_TS;
  pd = new_date_ts();
  bzero((char *)pd, sizeof(s_date_ts));
  yr = yr0;
  mm = JANUARY;
  /*Find the year*/
  while (jj <= ijulian) {
    jj = TS_calculate_jour_julien(++yr, yr0, mm);
  }
  jj = TS_calculate_jour_julien(--yr, yr0, mm);
  /*Find the month*/
  while (jj <= ijulian) {
    jj = TS_calculate_jour_julien(yr, yr0, mm++);
  }
  // mm--;
  jj = TS_calculate_jour_julien(yr, yr0, --mm);
  /*Find the day*/
  dd = ijulian - TS_calculate_jour_julien(yr, yr0, --mm); // il faut decaler de 1 pour que janvier soit le premier mois coherent avec la liste de param_ts pour laquelle JANVIER = 0
  // dd=ijulian-TS_calculate_jour_julien(yr,yr0,(mm-1));
  pd->dd = ++dd;
  pd->mm = mm; // il faut decaler de 1 pour que janvier soit le premier mois coherent avec la liste de param_ts pour laquelle JANVIER = 0
  pd->yyyy = yr;
  return pd;
}

/** \fn s_date_ts *TS_convert_julian2date_decimal(double zjulian,int yr0,FILE *flog)
 * \brief converts a julian day (integer) to a date(jj/mm/yyyy hh:mm:ss) that is put in a date pointer.
 *
 *\return  the pointer to the date
 */
s_date_ts *TS_convert_julian2date_decimal(double zjulian, int yr0, FILE *flog) {
  int yr;
  int jj = 0, mm;
  int dd = 0;
  int hh, min, ss;
  int stmp;
  int ijulian;
  s_date_ts *pd;
  int itime, itmp;
  double tmp;

  if (yr0 == CODE_TS)
    yr0 = INITIAL_YEAR_JULIAN_DAY_TS;
  ijulian = floor(zjulian);
  pd = TS_convert_julian2date(ijulian, yr0, flog); /*calculates first the integer part of the date*/
  tmp = zjulian - ijulian;

  tmp *= NSEC_DAY_TS;
  itime = (int)round(tmp);

  tmp = (double)itime;
  tmp *= CONV_SEC_HOUR_TS;

  hh = floor(tmp);
  itime -= hh * NSEC_HOUR_TS;

  tmp = (double)itime;
  tmp *= CONV_SEC_MIN_TS;

  min = floor(tmp);
  itime -= min * NSEC_MIN_TS;
  ss = floor(itime);

  pd->hh = hh;
  pd->min = min;
  pd->ss = ss;
  return pd;
}

/** \fn s_carac_ts *TS_carac_ts(s_ft *pft,int yr0,FILE *flog)
 * \brief creates a carac_ts ts structur that contains the beginning and ending dates as well as the number of years covered by the ts.
 *
 *\return  the pointer to carac_ts
 */
s_carac_ts *TS_carac_ts(s_ft *pft, int yr0, FILE *flog) {
  int yr[NEXTREMA_TS];
  s_ft *pftmp;
  double zjul;
  int i;
  s_carac_ts *pcts;

  pcts = new_carac_ts();
  bzero((char *)pcts, sizeof(s_carac_ts));

  for (i = BEGINNING_TS; i < NEXTREMA_TS; i++) {
    pftmp = TS_browse_ft(pft, i);
    zjul = pftmp->t;
    pcts->julian_day[i] = zjul;
    pcts->pd[i] = TS_convert_julian2date_decimal(zjul, yr0, flog);
    yr[i] = pcts->pd[i]->yyyy;
  }

  pcts->nyr = yr[END_TS] - yr[BEGINNING_TS] + 1;
  return pcts;
}

/** \fn s_ft **TS_cut_ts_annually(s_ft *pft,int yr0,FILE *flog)
 * \brief converts a julian day to a date(jj/mm/yyyy) that is put in a date pointer.
 *
 *\return  the pointer to the date
 */
s_ft **TS_cut_ts_annually(s_ft *pft, int yr0, FILE *flog) {
  s_ft **pfty;
  s_carac_ts *pcts;

  pcts = TS_carac_ts(pft, yr0, flog);
  pfty = ((s_ft **)calloc(pcts->nyr, sizeof(s_ft *)));
  // LP_printf(flog,"month beginning : %2d month_end : %2d\n",pcts->pd[BEGINNING_TS]->mm,(pcts->pd[END_TS]->mm));
  TS_create_annual_ts(pft, pfty, flog, CODE_TS, pcts->pd[BEGINNING_TS]->yyyy, pcts->pd[END_TS]->yyyy, pcts->pd[BEGINNING_TS]->mm, (pcts->pd[END_TS]->mm + 1)); // NF 28/10/2014 There is a problem with the values of the months which should follow the list in param_ts and then be transformed only for printing
  TS_destroy_carac_ts(pcts);                                                                                                                                   // NF 28/10/2014 to avoid memory leaks
  return pfty;
}

void print_2digit(FILE *fp, int ival) {

  if (ival >= 10)
    fprintf(fp, "%2d", ival);
  else
    fprintf(fp, "0%d", ival);
}

/**\fn void TS_print_date_from_julian(double t,int yr0,int icode,FILE *fp,FILE *flog)
 *\brief prints julian day t as a date in the FR_TS format jj/mm/yyyy or US_TS format yyyy/mm/dd in the file *fp
 * if icode=CODE_TS set to FR_TS by default
 */
void TS_print_date_from_julian(double t, int yr0, int icode, FILE *fp, FILE *flog) {
  s_date_ts *pd;
  pd = TS_convert_julian2date_decimal(t, yr0, flog);
  TS_print_date(pd, icode, fp, flog);
}

/**\fn void TS_print_date(s_date_ts *pd,int icode,FILE *fp,FILE *flog)
 *\brief prints the date in the FR_TS format jj/mm/yyyy or US_TS format yyyy/mm/dd in the file *fp
 * if icode=CODE_TS set to FR_TS by default
 */
void TS_print_date(s_date_ts *pd, int icode, FILE *fp, FILE *flog) {

  char sep[STRING_LENGTH_LP];
  sprintf(sep, "/");
  TS_print_date_sep(pd, icode, sep, fp, flog);
}

/**\fn void TS_print_date_sep(s_date_ts *pd,int icode,char sep,FILE *fp,FILE *flog)
 *\brief prints the date with a specific separator sep (a single character) in the FR_TS format jj"sep"mm"sep"yyyy or US_TS format yyyy"sep"mm"sep"dd in the file *fp
 * if icode=CODE_TS set to FR_TS by default
 */
void TS_print_date_sep(s_date_ts *pd, int icode, char *sep, FILE *fp, FILE *flog) {
  int month_num; // NF 28/10/2014 for numbers month_num=pd->mm+1 because the list of months startes at 0 and not 1.

  month_num = pd->mm + 1;
  if (icode == CODE_TS)
    icode = FR_TS;
  switch (icode) {
  case FR_TS:
    print_2digit(fp, pd->dd);
    fprintf(fp, "%s", sep);
    print_2digit(fp, month_num);
    fprintf(fp, "%s%4d", sep, pd->yyyy);
    break;
  case US_TS:
    fprintf(fp, "%4d%s", pd->yyyy, sep);
    print_2digit(fp, month_num);
    fprintf(fp, "%s", sep);
    print_2digit(fp, pd->dd);
    break;
  default:
    LP_error(flog, "In libts%4.2f TS_print_date() in %s line %d: Type of date encoding unknown\n", VERSION_TS);
    break;
  }
}

/**\fn void TS_print_time(s_date_ts *pd,FILE *fp,FILE *flog)
 *\brief prints the time in hh:mm:ss format in the file *fp
 *
 */
void TS_print_time(s_date_ts *pd, FILE *fp, FILE *flog) {
  print_2digit(fp, pd->hh);
  fprintf(fp, ":");
  print_2digit(fp, pd->min);
  fprintf(fp, ":");
  print_2digit(fp, pd->ss);
}

/**\fn void TS_print_time_hh_mm(s_date_ts *pd,FILE *fp,FILE *flog)
 *\brief prints the time in hh:mm format in the file *fp
 *
 */
void TS_print_time_hh_mm(s_date_ts *pd, FILE *fp, FILE *flog) {
  print_2digit(fp, pd->hh);
  fprintf(fp, ":");
  print_2digit(fp, pd->min);
}

/** \fn void TS_destroy_carac_ts(s_carac_ts *pcts)
 * \brief detroys a structur s_carac_ts by freeing the memory
 *
 *\return  void
 */
void TS_destroy_carac_ts(s_carac_ts *pcts) {
  int i;
  s_date_ts *pd;

  for (i = 0; i < NEXTREMA_TS; i++) {
    pd = pcts->pd[i];
    free(pd);
  }
  free(pcts);
}

double TS_unit2sec(int unit_dt, FILE *flog) {

  double dt_sec;
  dt_sec = 1;
  switch (unit_dt) {
  case SEC_TS:
    dt_sec = 1.;
    break;
  case MIN_TS:
    dt_sec = NSEC_MIN_TS;
    break;
  case HOUR_TS:
    dt_sec = NSEC_HOUR_TS;
    break;
  case DAY_TS:
    dt_sec = NSEC_DAY_TS;
    break;
  default:
    LP_error(flog, "libts%4.2f TS_unit2sec in %s line %d: Type of date encoding unknown\n", VERSION_TS);
    break;
  }
  return dt_sec;
}

/** \fn int TS_date2julian_dd(s_date_ts *pd,int yr0,FILE *flog)
 * \brief converts a date into a julian day (integer)
 *
 *\return  void
 */
int TS_date2julian_dd(s_date_ts *pd, int yr0, FILE *flog) {

  int yyyy, mm, dd, jj;

  if (yr0 == CODE_TS)
    yr0 = INITIAL_YEAR_JULIAN_DAY_TS;

  yyyy = pd->yyyy;
  mm = pd->mm;
  dd = pd->dd;
  jj = TS_calculate_jour_julien_j(yyyy, yr0, --mm, dd, flog); // NF 3/12/2020 it must be yr0 and not INITIAL_YEAR_JULIAN_DAY_TS

  return jj;
}

/** \fn int TS_date2julian_dd_hm(s_date_ts *pd,int yr0,FILE *flog)
 * \brief converts a date into a decimal julian day
 *
 *\return  void
 */
double TS_date2julian_dd_hm(s_date_ts *pd, int yr0, FILE *flog) {

  int hh, min, ss, jj;
  double t;

  if (yr0 == CODE_TS)
    yr0 = INITIAL_YEAR_JULIAN_DAY_TS;

  t = (double)TS_date2julian_dd(pd, yr0, flog);
  hh = pd->hh;
  min = pd->min;
  ss = pd->ss;
  t += TS_calc_julian_decimal(hh, min, ss, flog);

  return t;
}

double TS_convert_seconds2days(double tseconds, FILE *flog) // NF 3/12/2020 converts seconds in days. Usefull to use the fonctionnality of libts in a code where time is counted in seconds
{
  double tdays;
  tdays = tseconds * CONV_SEC_DDEC_TS;
  return tdays;
}
